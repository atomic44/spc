import re
from printcolor import printc
import math
import pyphen

dic = pyphen.Pyphen(lang='cs_CZ')


class BeautifulTable:
    def __init__(self, table_array, **kwargs):
        self._table_array = table_array

        self._clean_table_array()

        if "padding" in kwargs:
            self._padding = kwargs["padding"]
        else:
            self._padding = 2

        self._padding_char = "{0: <{1}}".format("", self._padding)

        if "max_width" in kwargs:
            self._max_width = kwargs["max_width"]
        else:
            self._max_width = 78

        if "max_cols_num" in kwargs:
            self._max_cols_num = kwargs["max_cols_num"]
        else:
            self._max_cols_num = 15

        self._rows_num = self._get_rows_num()
        self._cols_num = self._get_cols_num()

        self._hyphen_char = "-"

        if "parameters" in kwargs:
            self._parameters = kwargs["parameters"]

        self._rowspan_hyphen_char = '-'
        self._rowspan_hyphen = True

        if "wrap_widths" in self._parameters:
            self._wrap_widths = self._parameters["wrap_widths"]
        else:
            self._wrap_widths = self._compute_wrap_widths()

    def convert_to_string(self):

        if not self._table_array:
            return ""

        self._compute_longest_cols_and_rows_and_wrap_cells()
        rows_array = self._make_beautiful_table()
        self._compute_table_line()
        table_str = self._insert_lines_and_hyphens(rows_array)

        return table_str

    def _get_rows_num(self):
        return len(self._table_array)

    def _get_cols_num(self):
        cols_num = 0
        for row in self._table_array:
            if len(row) > cols_num:
                cols_num = len(row)

        if cols_num > self._max_cols_num:
            printc.error("Maximum columns reached", str(cols_num))
            return None
        if cols_num <= 0:
            printc.error("Incorrect columns number.", str(cols_num))
            return None

        return cols_num

    def _clean_table_array(self):

        # delete blank columns from end
        max_columns = 0
        for i in range(len(self._table_array)):
            first_blank_col = len(self._table_array[i])
            for j in range(len(self._table_array[i]))[::-1]:
                # printc.blue(self._table_array[i][j]["string"])
                # printc.red(str(self._table_array[i][j]["parameters"]))
                if "colspan" not in self._table_array[i][j]["parameters"] and "rowspan" not in self._table_array[i][j]["parameters"]:
                    if re.match("^\s*$", self._table_array[i][j]["string"]):
                        first_blank_col = j
                    else:
                        break
                else:
                    break
            self._table_array[i] = self._table_array[i][:first_blank_col]
            if first_blank_col > max_columns:
                max_columns = first_blank_col

        # filter empty rows
        self._table_array = [x for x in self._table_array if x]

        # make all rows have same number of cols
        for i in range(len(self._table_array)):
            if len(self._table_array[i]) > max_columns:
                printc.error("Max columns are counted badly.")
            while len(self._table_array[i]) < max_columns:
                self._table_array[i].append({"string": "", "parameters": {}})
        # remove empty columns
        # TODO buggy, of course
        """
        empty_columns = []
        for j in range(max_columns):
            for i in range(len(self._table_array)):
                if "colspan" not in self._table_array[i][j]["parameters"] or "rowspan" not in self._table_array[i][j]["parameters"]:
                    if not re.match("^\s*$", self._table_array[i][j]["string"]):
                        break
            else:
                empty_columns.append(j)
        if empty_columns:
            for i in range(len(self._table_array)):
                row = []
                for j in range(len(self._table_array[i])):
                    if j in empty_columns:
                        continue
                    else:
                        row.append(self._table_array[i][j])
                self._table_array[i] = row
        """

        return self._table_array

    def _compute_wrap_widths(self):
        padding_sum = (self._cols_num - 1) * self._padding

        wrap_width = math.floor((self._max_width - padding_sum) / self._cols_num)

        wrap_widths = [wrap_width] * self._cols_num
        wrap_widths_sum = sum(wrap_widths)
        i = 0
        while wrap_widths_sum != (self._max_width - padding_sum):
            wrap_widths[i] += 1
            i += 1
            i %= self._cols_num
            wrap_widths_sum = sum(wrap_widths)

        return wrap_widths

    def _wrap_paragraph(self, text, width, hyphenator, hyphen='-'):
        if "wrap_before" in self._parameters:
            for wrap_pattern in self._parameters["wrap_before"]:
                text = re.sub('([^\n])(' + wrap_pattern + ')', '\g<1>\n\g<2>', text)
        if "wrap_after" in self._parameters:
            for wrap_pattern in self._parameters["wrap_after"]:
                text = re.sub('([^\n])(' + wrap_pattern + ')', '\g<1>\n\g<2>', text)

        paragraphs_array = text.split('\n')
        final_array = []

        for paragraph in paragraphs_array:
            text_array = paragraph.split(' ')

            row = []
            row_len = 0
            # print(text_array)
            # width = 3
            # print("width", width)
            # TODO this is a real mess..Do sth about it!!!
            for w, word in enumerate(text_array):
                if word[:1] == "\n":
                    if row:
                        final_array.append(' '.join(row))
                    row.clear()
                    row_len = 0
                    word = word[1:]
                if row_len + len(word) + len(row) <= width:  # if it fits to one row
                    row.append(word)
                    row_len += len(word)
                else:  # if it doesn't fit to one row
                    if row_len + len(row) > math.floor(width / 3):
                        final_array.append(' '.join(row))
                        row.clear()
                        row_len = 0
                        text_array.insert(w + 1, word)
                        continue
                    hyph_pos = hyphenator.positions(word)
                    for i in reversed(hyph_pos):  # take it from longest
                        index = i
                        if row_len + len(row) + index + len(hyphen) > width:
                            continue
                        else:
                            break
                    else:  # if it cannot be broken
                        if hyph_pos and len(word) <= width:
                            final_array.append(' '.join(row))
                            row.clear()
                            row_len = 0
                            text_array.insert(w + 1, word)
                            continue
                        elif len(word) > width:
                            while True:
                                free_chars = width - (row_len + len(row) + len(hyphen))
                                if free_chars < 1:
                                    final_array.append(' '.join(row))
                                    row.clear()
                                    row_len = 0
                                else:
                                    break
                            row.append(word[:free_chars] + hyphen)
                            final_array.append(' '.join(row))
                            row.clear()
                            row_len = 0
                            text_array.insert(w + 1, word[free_chars:])
                            continue
                        else:
                            if row:
                                final_array.append(' '.join(row))
                            row.clear()
                            row_len = 0
                            row.append(word)
                            row_len += len(word)
                            continue

                    row.append(word[:index] + hyphen)
                    final_array.append(' '.join(row))
                    row.clear()
                    row_len = 0
                    text_array.insert(w + 1, word[index:])
                    # row_len += len(word[index:])

            final_array.append(' '.join(row))
            # print(final_array)
            longest_row = 0
            for row in final_array:
                if len(row) > longest_row:
                    longest_row = len(row)

        final_paragraph = '\n'.join(final_array)
        return longest_row, final_paragraph

    def _compute_longest_cols_and_rows_and_wrap_cells(self):
        # TODO fuck this began being reallly messy...should be splitted..
        highest_cols = [0] * self._rows_num
        widest_cols = [0] * self._cols_num
        for row_it in range(len(self._table_array)):
            for col_it in range(len(self._table_array[row_it])):
                # print("[" + str(i) + "][" + str(j) + "] " + "'" + html_array[i][j] + "'")
                # print(wrap_width)
                cell = self._table_array[row_it][col_it]
                string = cell["string"]

                wrap_width = self._wrap_widths[col_it]
                if "colspan" in cell["parameters"] and "begin" in cell["parameters"]["colspan_pos"]:
                    colspan_width = cell["parameters"]["colspan_width"]
                    for i in range(colspan_width - 1):
                        wrap_width += self._padding
                        wrap_width += self._wrap_widths[col_it+i]
                col_width, string = self._wrap_paragraph(string, wrap_width, dic)
                if col_width > 0:
                    if "colspan" in cell["parameters"] and "begin" in cell["parameters"]["colspan_pos"]:
                        col_width_without_padding = col_width - ((colspan_width - 1) * self._padding)
                        base_width = col_width_without_padding // colspan_width
                        base_widths = [base_width] * colspan_width
                        base_sum = base_width * colspan_width
                        modulo = len(base_widths)
                        counter = 0
                        while base_sum != col_width_without_padding:
                            base_widths[counter] += 1
                            counter += 1
                            counter %= modulo
                            base_sum += 1
                        # print(string)
                        # printc.red(str(base_widths))
                        # printc.blue(str(col_width))

                        for i in range(colspan_width):
                            if widest_cols[col_it+i] < base_widths[i]:
                                widest_cols[col_it+i] = base_widths[i]
                    else:
                        if widest_cols[col_it] < col_width:
                            widest_cols[col_it] = col_width

                col_height = len(string.split('\n'))
                if highest_cols[row_it] < col_height:
                    highest_cols[row_it] = col_height

                self._table_array[row_it][col_it]["string"] = string

            # make sure every row has the same numbers of cols
            while True:
                if len(self._table_array[row_it]) < self._cols_num:
                    self._table_array[row_it].append({"string": ""})
                else:
                    break

            self._highest_cols = highest_cols
            self._widest_cols = widest_cols

    def _table_row_to_str(self, row_array, big_row_it):

        for col_it in range(len(row_array)):
            full_len = self._widest_cols[col_it]
            for row_it in range(len(row_array[col_it]["string_array"])):
                if "colspan" in row_array[col_it]["parameters"] and "begin" not in \
                        row_array[col_it]["parameters"]["colspan_pos"]:
                    continue
                elif "colspan" in row_array[col_it]["parameters"] and "begin" in \
                        row_array[col_it]["parameters"]["colspan_pos"]:
                    for col in range(row_array[col_it]["parameters"]["colspan_width"] - 1):
                        full_len += self._padding
                        full_len += self._widest_cols[col_it + col]

                str_len = len(row_array[col_it]["string_array"][row_it])
                diff = full_len - str_len
                # TODO colspan inside rowspan not working
                try:
                    row_array[col_it]["string_array"][row_it] += "{0: <{1}}".format("", diff)
                except ValueError as e:
                    printc.red(str(e))
                    continue
            # print(self._longest_cols[i], len(row_array[col_it]))
            # make sure all cols have same rows
            while len(row_array[col_it]["string_array"]) < self._highest_cols[big_row_it]:
                row_array[col_it]["string_array"].append("{0: <{1}}".format("", self._widest_cols[col_it]))
        # print("-")
        joined_rows = []
        # inverse big_row_array
        for row in range(len(row_array)):
            row_array[row] = row_array[row]["string_array"]
        row_array = list(zip(*row_array))
        # printc.blue(str(row_array))
        for row_it in range(self._highest_cols[big_row_it]):
            joined_rows.append(self._padding_char.join(row_array[row_it]))

        final_row = '\n'.join(joined_rows)
        return final_row

    @staticmethod
    def _make_big_row_array(row):
        big_row_array = []
        for j in range(len(row)):
            string_array = row[j]["string"].split('\n')
            parameters = row[j]["parameters"]
            big_row_array.append(
                {"string_array": string_array, "parameters": parameters})
        return big_row_array

    def _make_beautiful_table(self):
        table_array = []
        rowspan_rows = 0

        for i in range(len(self._table_array)):
            if rowspan_rows > 1:
                rowspan_rows -= 1
                continue

            rowspan = False
            for j in range(len(self._table_array[i])):
                if "rowspan" in self._table_array[i][j]["parameters"]:
                    if "begin" in self._table_array[i][j]["parameters"]["rowspan_pos"]:
                        rowspan = True
                        if self._table_array[i][j]["parameters"]["rowspan_height"] > rowspan_rows:
                            rowspan_rows = self._table_array[i][j]["parameters"]["rowspan_height"]

            if rowspan:
                final_rowspan_table = []
                for x in range(len(self._table_array[i])):
                    final_rowspan_table.append({"string": "", "parameters": {}})

                for rowspan_it in range(rowspan_rows):
                    max_row_height = 1
                    rowspan_table = self._table_array[i+rowspan_it]
                    for j in range(len(rowspan_table)):
                        if "rowspan" in rowspan_table[j]["parameters"]:
                            if "end" not in rowspan_table[j]["parameters"]["rowspan_pos"]:
                                continue
                        row_height = len(rowspan_table[j]["string"].split('\n'))
                        if row_height > max_row_height:
                            max_row_height = row_height

                    for j in range(len(rowspan_table)):
                        if "rowspan" not in rowspan_table[j]["parameters"]:
                            row_height = len(rowspan_table[j]["string"].split('\n'))
                            while row_height != max_row_height:
                                rowspan_table[j]["string"] += '\n'
                                row_height += 1
                            if self._rowspan_hyphen and rowspan_it != rowspan_rows-1:
                                rowspan_table[j]["string"] += '\n' + self._rowspan_hyphen_char + '\n'
                    # printc.cyan(str(max_row_height))
                    # printc.magenta(str(rowspan_table))

                    for j in range(len(final_rowspan_table)):
                        final_rowspan_table[j]["string"] += rowspan_table[j]["string"]

                # printc.yellow(str(final_rowspan_table))
                table_row = final_rowspan_table
            else:
                table_row = self._table_array[i]
                # for row_it in range(rowspan_rows):

            big_row_array = self._make_big_row_array(table_row)
            final_row = self._table_row_to_str(big_row_array, i)
            table_array.append(final_row)

        # for row in table_array:
        #     printc.red(str(row))
        #     print('-')
        return table_array

    def _compute_table_line(self):
        # put whole table in one final string
        table_width = 0
        for row_len in self._widest_cols:
            table_width += row_len
        table_width += (self._cols_num - 1) * self._padding
        self._table_line = "{0:-<{1}}".format("", table_width)

    def _insert_lines_and_hyphens(self, rows_array):
        if "line-after" not in self._parameters:
            self._parameters["line-after"] = [1]
        if "no-hyphens" not in self._parameters and "hyphens" not in self._parameters:
            for long_col in self._highest_cols[1:]:
                if long_col > 1:
                    self._parameters["hyphens"] = True
                    break

        new_array = rows_array
        # printc.red(str(len(rows_array)))
        offset = 0
        for i in range(len(rows_array))[1:]:
            if i in self._parameters["line-after"]:
                new_array.insert(i + offset, self._table_line)
                offset += 1
            elif "hyphens" in self._parameters:
                new_array.insert(i + offset, self._hyphen_char)
                offset += 1
        table_str = '\n'.join(new_array)
        table_str = '\n'.join((self._table_line, table_str, self._table_line, ""))
        return table_str
