#!/usr/bin/python3
# coding=utf-8
import sys  # using system arguments
import os  # get base filename from path
import csv  # csv reader/writer
import argparse
import shutil  # for removing directory tree, force move
import re  # regular expressions
import hashlib  # for checking duplicate files
import logging  # debugging messages
from io import StringIO
from collections import defaultdict
import subprocess
import math
import xlrd  # xls to csv
import requests  # for downloading files
from unidecode import unidecode  # for removing accents
from bs4 import BeautifulSoup  # working with html DOM
from colorlog import ColoredFormatter
import pyperclip
from printcolor import printc
from beautifultable import BeautifulTable
from collections import OrderedDict
from difflib import SequenceMatcher
import itertools

logs = StringIO()
word_path_cygwin = "/cygdrive/c/Program Files/Microsoft Office 15/root/office15/winword.exe"
word_path_win = r"C:\Program Files\Microsoft Office 15\root\office15\winword.exe"
word_macro_name = "BatchSaveAs"
diff_path_cygwin = "/cygdrive/c/Program Files/KDiff3/kdiff3.exe"
diff_path_win = r"C:\Program Files\KDiff3\kdiff3.exe"
sublime_path_cygwin = "/cygdrive/c/Program Files/Sublime Text 3/sublime_text.exe"
sublime_path_win = r"C:\Program Files\Sublime Text 3\sublime_text.exe"
PDF_DIR = "PDF"
HTML_DIR = "HTML"
DWNLD_DIR = PDF_DIR + os.sep + "download"
EMEA_DIR = DWNLD_DIR + os.sep + "emea"
TXT_DIR = "TXT"
TMP_FILENAME = "tmp"
OLD_DIR = DWNLD_DIR + os.sep + "old"
CHAPTER_ERROR = 0
far_dict = None
far_csv_glob = ""
nu_width = 60

table_begin_str = "# table-"
table_end_str = "##"
col_char = "\n> "
row_char = "\n-"
hyphen_char = "-"
begin_boundaries_string = "---"
end_boundaries_string = "\n---"
colspan_begin_char = "<<"
colspan_end_char = ">>"
rowspan_begin_char = "^^"
rowspan_end_char = "vv"

years_list = set()
print_format = "{:>3}  {:<7}  {:<40}  {:<15}  {:<12}"
emea_prefix = "[emea]-"
status = {"downloaded": "downloaded", "duplicate": "duplicate",
          "invalid_sukl_code": "invalid SUKL",
          "missing_spc_link": "missing link", "broken_spc_link": "broken link",
          "pi_reg_num": "par. import", "emea": "emea", "error": "error",
          "old": "old", "converted_to_txt": "converted to txt",
          "converted_to_html": "converted to html"}


class Table:
    def __init__(self, parameters=None):
        if parameters is None:
            self.parameters = {"type": "htmltab"}
        else:
            self.parameters = parameters

    num = 0
    html = ""
    str = ""
    start = 0
    end = 0
    start_len = 0
    end_len = 0
    regex = ""
    raw = False


class SPC:
    num = 0
    sukl_code = ""
    name = ""
    reg_num = ""
    year = ""
    filename = ""
    downloaded = ""
    type = ""
    url = ""
    duplicate = ""
    status = ""
    done_now = False
    old_status = ""

    def __str__(self):
        return print_format.format(str(self.num), self.sukl_code,
                                   self.name[:40], self.reg_num, self.status)


def cmd_help():
    printc.info("Usage: " + str(sys.argv[0]) + " <filename>.xls(x)")
    printc.warning("There should be some more help.")


def count_files_in_dir(directory):
    cnt = 0
    for path, dirs, files in os.walk(directory):
        for _ in files:
            cnt += 1
    return cnt


def chunk_reader(fobj, chunk_size=1024):
    """Generator that reads a file in chunks of bytes"""
    while True:
        chunk = fobj.read(chunk_size)
        if not chunk:
            return
        yield chunk


def filename_exists(root, filename):
    for path, dirs, files in os.walk(root):
        if filename in files:
            return True
    else:
        return False


def url_in_list(url, spc_list):
    for spc in spc_list:
        if url == spc.url:
            return spc
    else:
        return None


def find_duplicates(path):
    duplicates_list = {}
    used_hash = hashlib.sha1
    hashes = {}
    for file in os.listdir(path):
        if not os.path.isfile(os.path.join(path, file)):
            continue

        new_file = os.path.join(path, file)
        hashobj = used_hash()
        for chunk in chunk_reader(open(new_file, 'rb')):
            hashobj.update(chunk)
        file_id = (hashobj.digest(), os.path.getsize(new_file))
        unique_file = hashes.get(file_id, None)
        if unique_file:
            # print("\nFound duplicate of:", unique_file)
            # printc.warning("------------------>", new_file)
            new_filename = os.path.split(new_file)[1]
            unique_filename = os.path.split(unique_file)[1]
            duplicates_list[new_filename] = unique_filename
        else:
            hashes[file_id] = new_file
    return duplicates_list


def check_if_identical(filepath1, filepath2):
    """
    Check if two files are identical or not.
    Need to provide path to file.
    Returns True if are identical, False if not.
    """
    used_hash = hashlib.sha1
    hashes = {}
    files = [filepath1, filepath2]
    for file in files:
        hashobj = used_hash()
        for chunk in chunk_reader(open(file, 'rb')):
            hashobj.update(chunk)
        file_id = (hashobj.digest(), os.path.getsize(file))
        duplicate = hashes.get(file_id, None)
        if duplicate:
            return True
        else:
            hashes[file_id] = file

    return False


def clear_folder_except(exceptions):
    """clear folder from everything except specified files"""
    delete_list = os.listdir('.')
    for filename in delete_list:
        filetype = os.path.splitext(filename)[1][1:]  # remove dot
        if filetype not in exceptions:
            if os.path.isdir(filename):
                while True:
                    try:
                        shutil.rmtree(filename)  # remove non-empty directories
                    except PermissionError as e:
                        answer = query_retry_skip_abort(str(e))
                        if answer == "retry":
                            continue
                        elif answer == "skip":
                            break
                        else:
                            sys.exit(2)
                    else:
                        break
            else:
                os.remove(filename)
    printc.info("Removed everything except this filetypes: " + ", ".join(
        exceptions) + ".")


def rename_file_to_folder_name(filename):
    """rename file to name of the parent folder"""
    if not os.path.isfile(filename):
        printc.error("ERROR: File \"" + filename + "\" does not exist.")
        printc.info("Solution: Provide an argument with an existing filename.")
        sys.exit()

    work_dir = os.path.basename(
        os.getcwd())  # os.getcwd() returns path of current working directory
    if work_dir.find("SPC") == -1:  # didn't find SPC in the work_dir string
        printc.error(
            "ERROR: Bad name of working directory. You're not in a directory named \"SPC[xx]\"")
        answer = query_yes_no("Do you want to proceed?", "no")
        if not answer:
            sys.exit()

    filename_without_ext = os.path.splitext(filename)[0]
    if filename_without_ext == work_dir:
        return filename

    filetype = os.path.splitext(filename)[1]  # returns filetype with dot
    new_filename = work_dir + filetype
    if os.path.isfile(new_filename):
        printc.warning(
            "WARNING: File \"" + new_filename + "\" already exists.")
        answer = query_yes_no("Do you want to overwrite?", "no")
        if not answer:
            sys.exit()

    while True:
        try:
            shutil.move(filename, new_filename)
        except PermissionError:
            answer = query_retry_skip_abort(
                "Renaming " + filename + " failed. Permission denied.\n" + "Maybe it's opened in other program? Close and retry")
            if answer == "retry":
                continue
            elif answer == "skip":
                return filename
            else:  # abort
                sys.exit()
        else:
            break

    return new_filename


def query_a_b(question, default="a", auto=False):
    valid = {"a": "a", "b": "b"}

    if auto and default:
        return valid[default]

    if default is None:
        prompt = " [a/b] "
    elif default == "a":
        prompt = " [A/b] "
    elif default == "b":
        prompt = " [a/B] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        printc.warning(question + prompt, end="")
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with"
                             "'a' or 'b'.\n")


def query_yes_no(question, default="yes", auto=False):
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}

    if auto and default:
        return valid[default]

    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        printc.warning(question + prompt, end="")
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def query_retry_skip_abort(question, default="retry", auto=False):
    valid = {"retry": "retry", "r": "retry", "skip": "skip", "s": "skip",
             "abort": "abort", "a": "abort"}

    if auto and default:
        return valid[default]

    if default is None:
        prompt = " [retry/skip/abort] "
    elif default == "retry":
        prompt = " [Retry/skip/abort] "
    elif default == "skip":
        prompt = " [retry/Skip/abort] "
    elif default == "abort":
        prompt = " [retry/skip/Abort] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        print(question, prompt, end="")
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'retry', 'skip' or 'abort' "
                             "(or 'r', 's' or 'a').\n")


def xls_to_csv(filename):
    """convert xls(x) to csv"""
    filetype = (os.path.splitext(filename)[1])  # returns filetype with dot
    if filetype.lower() != ".xls" and filetype.lower() != ".xlsx":
        printc.error("ERROR: Argument isn't xls(x) filetype.")
        printc.info("Solution: Provide Excel xls(x) file.")
        sys.exit(2)

    xls_obj = xlrd.open_workbook(filename, encoding_override='cp1250')

    if xls_obj.nsheets != 1:
        printc.warning("WARNING: There is more than one sheet.")
        answer = query_yes_no(
            "Continue? I will work with the first sheet only.", "no")
        if not answer:
            sys.exit()

    xls_sheet = xls_obj.sheet_by_index(0)
    base_filename = os.path.splitext(filename)[0]
    csv_filename = base_filename + ".csv"

    if os.path.isfile(csv_filename):
        answer = query_yes_no("WARNING: Csv file already exists. Overwrite?",
                              "no", True)
        if not answer:
            printc.warning("Using existing CSV file \"" + csv_filename + "\".")
            return csv_filename
        else:
            printc.warning("Overwriting \"" + csv_filename + "\".")

    csv_file = open(csv_filename, 'w', newline='', encoding='cp1250')
    csv_writer = csv.writer(csv_file, dialect='excel2013')

    for row_num in range(xls_sheet.nrows):
        csv_writer.writerow(xls_sheet.row_values(row_num))
    csv_file.close()

    printc.info(csv_filename + " created.")
    return csv_filename


def check_csv_header(header, string, optional=False):
    cnt = header.count(string)
    if cnt == 1:
        return header.index(string)
    if cnt < 1:
        if optional:
            return -1
        else:
            printc.error(
                "ERROR: CSV header does not contain \"" + string + "\".")
            printc.info(
                "Solution: Change header in CSV so  it contain \"" + string + "\".")
        sys.exit()
    if cnt > 1:
        printc.error(
            "ERROR: CSV header contain more than one \"" + string + "\" strings.")
        printc.info(
            "Solution: Change header in CSV so it contain only one \"" + string + "\" string.")
        sys.exit()


def filename_from_spc_name(spc_name, filetype):
    filename = spc_name.lower()
    filename = filename.replace("%", "proc")
    filename = filename.replace("+", "-plus-")
    filename = re.sub("[^\w\(\)]", "-", filename)
    filename = unidecode(filename)

    return filename + filetype


def do_not_overwrite(filepath):
    appendix = 1
    path, filename = os.path.split(filepath)
    filename_base, extension = os.path.splitext(filename)  # split extension

    regex_match = re.search("(.*)\((\d)\)$", filename_base)
    if regex_match:
        filename_base = regex_match.group(1)
        appendix = int(regex_match.group(2))
        filepath = os.path.join(path, filename_base) + extension
        print(filepath)

    while True:
        if os.path.isfile(filepath):
            filepath = (os.path.join(path, filename_base) + "(" + str(
                appendix) + ")" + extension)
            appendix += 1
        else:
            return filepath


def download(url):
    while True:
        try:
            response = requests.get(url, stream=True)
        except requests.exceptions.RequestException as e:
            printc.error(e)
            answer = query_retry_skip_abort(
                "\nERROR: Check your internet connection.", "retry")
            if answer == "retry":
                continue
            if answer == "abort" or "skip":
                return None
        else:
            return response


def already_downloaded(spc):
    if filename_exists(DWNLD_DIR, spc.filename):
        if spc.status == status["emea"]:
            filename = filename_from_spc_name(spc.name,
                                              os.path.splitext(spc.filename)[
                                                  1])
            if filename_exists(DWNLD_DIR, filename):  # without emea preffix
                return True
            else:
                return False
        else:
            return True
    else:
        return False


def download_spc_from_list(spc_list):
    printc.info("There is", str(len(spc_list)), "SPCs in list.")
    print_header()

    for spc in spc_list:
        # check if it is already downloaded (and if it is, check for file)
        if spc.status in (
                status["downloaded"], status["emea"], status["duplicate"],
                status["old"], status["converted_to_txt"],
                status["converted_to_html"]):
            if already_downloaded(spc):
                spc.done_now = True
                print(spc)
                continue
            else:
                logging.warning(
                    "Check as downloaded but missing (" + spc.filename + ").")
                spc.status = ""
                spc.url = ""
                spc.filename = ""

        if spc.status in (
                status["missing_spc_link"], status["invalid_sukl_code"],
                status["pi_reg_num"]):
            spc.done_now = True
            print(spc)
            continue

        sukl_url = "http://www.sukl.cz/modules/medication/search.php?" \
                   "data%5Bsearch_for%5D=&data%5Bcode%5D=" + spc.sukl_code + "&data%5Batc_group%5D=&data%5Bmaterial%5D=&data%5Bpath%5D=" \
                                                                             "&data%5Breg%5D=&data%5Bradio%5D=none&data%5Brc%5D=" \
                                                                             "&data%5Bchbox%5D%5B%5D=braill-yes&data%5Bchbox%5D%5B%5D=braill-no" \
                                                                             "&data%5Bchbox%5D%5B%5D=braill-def&data%5Bwith_adv%5D=1&" \
                                                                             "search=Vyhledat&data%5Blisting%5D=100"

        response = download(sukl_url)
        if not response:
            return
        elif not response.status_code == 200:
            logging.error("SUKL URL not reached. Check: " + sukl_url)
            spc.status = status["error"]
            print(spc)
            spc.done_now = True
            continue

        html = response.text
        soup = BeautifulSoup(html)
        # get all links from page
        all_td = soup.find_all("td", attrs={"headers": "spc"})
        if len(all_td) < 1:
            if "Žádná položka nevyhovuje zadaným kritériím" in html:
                spc.status = status["invalid_sukl_code"]
            else:
                logging.error(
                    "PDF url of \"" + spc.sukl_code + "\" not found. Check: " + sukl_url)
                spc.status = status["error"]
            print(spc)
            spc.done_now = True
            continue
        elif len(all_td) > 1:
            logging.error(
                "More PDF urls for \"" + spc.sukl_code + "\". Check: " + sukl_url)
            spc.status = status["error"]
            print(spc)
            spc.done_now = True
            continue

        # len(all_td) == 1
        link_td = all_td[0]

        if len(link_td.contents) > 1:
            pdf_url = link_td.contents[1].get('href')
        else:
            spc.status = status["missing_spc_link"]
            print(spc)
            spc.done_now = True
            continue

        # get reg. number to check
        parent_tr = link_td.parent
        reg_num_td = parent_tr.find("td", attrs={"headers": "regc"})
        reg_num = reg_num_td.get_text().strip().replace(" ", "")
        reg_num_match = re.match("\d{2}/\d{3,4}/\d{2}-[A-Z]/?[A-Z]?$", reg_num)
        if not reg_num_match:  # SUKL reg num check
            reg_num_match = re.match("EU/\d/\d{2}/\d{3}/\d{3}$", reg_num)
            if not reg_num_match:  # EMEA reg num check
                if "/PI/" in reg_num:
                    spc.status = status["pi_reg_num"]
                else:
                    logging.error(
                        "Wrong reg. num. pattern in \"" + spc.reg_num + "\". Check: " + sukl_url)
                    spc.status = status["error"]
                print(spc)
                spc.done_now = True
                continue

        # download PDF
        response = download(pdf_url)
        if not response:
            return
        if not response.status_code == 200:
            if response.status_code == 404:
                spc.status = status["broken_spc_link"]
                print(spc)
                spc.done_now = True
                continue
            else:
                logging.error("PDF link returns " + str(
                    response.status_code) + ". Check: ", sukl_url)
                spc.status = status["error"]
                print(spc)
                spc.done_now = True
                continue

        # find PDF filetype
        if "Content-disposition" in response.headers:
            filename = re.search("filename=(.*)", response.headers[
                "Content-Disposition"]).group(1)
        else:
            filename = re.search(".*/(.*)$", pdf_url).group(1)
        filetype = os.path.splitext(filename)[1]

        # make PDF filename from SPC name
        filename = filename_from_spc_name(spc.name, filetype)

        if spc.type == "SUKL":
            filepath = os.path.join(DWNLD_DIR, filename)
        else:  # EMEA
            filepath = os.path.join(EMEA_DIR, filename)
            empty_file = open(filepath, 'wb')
            empty_file.close()
            filename = emea_prefix + filename
            filepath = os.path.join(EMEA_DIR, filename)

            same_spc = url_in_list(pdf_url, spc_list)

            if same_spc:
                if filename_exists(DWNLD_DIR, same_spc.filename):
                    spc.url = same_spc.url
                    spc.filename = same_spc.filename
                    spc.status = status["emea"]
                    print(spc)
                    spc.done_now = True
                    continue

        # create new temporary file
        tmp_file = "tmp"
        pdf_file = open(tmp_file, 'wb')
        pdf_file.write(response.content)
        pdf_file.close()

        # if it already exists
        if os.path.isfile(filepath):  # csv new and folder not cleaned
            if check_if_identical(filepath, tmp_file):
                os.remove(tmp_file)
                if spc.type == "SUKL":
                    spc.status = status["downloaded"]
                else:
                    spc.status = status["emea"]
                print(spc)
                spc.done_now = True
                continue
            else:  # csv old and folder is cleaned
                filepath = do_not_overwrite(filepath)

        # if it doesn't exist
        os.rename(tmp_file, filepath)
        spc.filename = filename
        spc.url = pdf_url
        if spc.type == "SUKL":
            spc.status = status["downloaded"]
        else:
            spc.status = status["emea"]
        print(spc)
        spc.done_now = True


def read_csv(csv_filename):
    """read CSV file and download PDF/doc from SUKL/EMEA and write it to CSV"""

    # if os.path.isfile(csv_filename):
    # answer = query_yes_no("CSV file already exists. Do you want to overwrite?", "no")
    # if not answer:
    #     sys.exit()
    csv_file = open(csv_filename, 'r', encoding='cp1250')
    csv_reader = csv.reader(csv_file, dialect='excel2013')
    header = next(csv_reader)  # read only one line

    header_index = {'sukl_code': check_csv_header(header, "kod_sukl"),
                    'name': check_csv_header(header, "naz_obch"),
                    'reg_num': check_csv_header(header, "rc"),
                    'year': check_csv_header(header, "rok", True),
                    'filename': check_csv_header(header, "nazev souboru",
                                                 True),
                    'downloaded': check_csv_header(header, "stazeno", True),
                    'missing_url': check_csv_header(header, "chybejici odkaz",
                                                    True),
                    'duplicate': check_csv_header(header, "stejne spc", True),
                    'url': check_csv_header(header, "url", True),
                    'status': check_csv_header(header, "status", True)}

    spc_list = []
    names_list = set()

    num = 0
    line = 0
    for row in csv_reader:
        line += 1
        if not row:  # skip empty lines
            continue
        num += 1
        spc = SPC()
        spc.num = num
        spc.sukl_code = row[header_index['sukl_code']]
        name = row[header_index['name']]
        if name in names_list:
            name += " (1)"
        else:
            names_list.add(name)
        spc.name = name
        spc.reg_num = row[header_index['reg_num']]
        if header_index['status'] != -1:
            spc.status = row[header_index['status']]
        if header_index['year'] != -1:
            spc.year = row[header_index['year']]
            years_list.add(spc.year)
        if header_index['filename'] != -1:
            spc.filename = row[header_index['filename']]
        if header_index['downloaded'] != -1:
            spc.downloaded = row[header_index['downloaded']]
        if header_index['missing_url'] != -1:
            spc.missing_url = row[header_index['missing_url']]
        if header_index['url'] != -1:
            spc.url = row[header_index['url']]
        if header_index['duplicate'] != -1:
            spc.duplicate = row[header_index['duplicate']]

        match_obj = re.match("\d{2}/\d{3,4}/\d{2}-([A-Z]/?[A-Z]?$)",
                             spc.reg_num)
        if match_obj:
            spc.type = "SUKL"
        else:
            match_obj = re.match("EU/\d/\d{2}/\d{3}/\d{3}$", spc.reg_num)
            if match_obj:
                spc.type = "EMEA"
            else:
                # print(line, "Bad registration number pattern")
                logging.warning(
                    "Bad registration number pattern in \"" + spc.reg_num + "\" at line" + str(
                        line) + ".")
                continue

        spc.old_status = spc.status
        spc_list.append(spc)

    csv_file.close()
    return spc_list


def print_header():
    header = print_format.format("row", "sukl", "name", "reg. no", "status")
    header_len = len(header)

    print("{0:=<{1}}".format("", header_len))
    print(header)
    print("{0:=<{1}}".format("", header_len))


def print_footer(counts):
    footer_len = len(print_format.format("", "", "", "", ""))
    print("{0:=<{1}}".format("", footer_len))

    for key, value in sorted(counts.items()):
        print("{:.<15} {:<3}  ({} new)".format(key, value[0],
                                               value[0] - value[1]))

    print("{0:=<{1}}".format("", footer_len))


def check_sum(spc_list):
    old_count = {}
    count = {}

    for key, value in status.items():
        count[value] = 0
        old_count[value] = 0

    done_now_cnt = 0
    unique_emea_urls = set()
    for spc in spc_list:
        if spc.status:
            count[spc.status] += 1
            if "europa.eu" in spc.url:  ## check EMEA URL
                unique_emea_urls.add(spc.url)
        if spc.done_now:
            done_now_cnt += 1
        if spc.old_status:
            old_count[spc.old_status] += 1

    checksum = 0
    for cnt in count.values():
        checksum += cnt

    if done_now_cnt != checksum:
        logging.error("CHECKSUM failed. Done now: " + str(
            done_now_cnt) + ", checksum: " + str(checksum) + ".")
    else:
        logging.info("CHECKSUM ok (" + str(checksum) + " SPCs done).")

    count["extra emea"] = len(unique_emea_urls)
    old_count["extra emea"] = count["extra emea"]

    downloaded_files_cnt = count[status["downloaded"]] + count[
        status["emea"]] + count["extra emea"]
    files_in_folder_cnt = count_files_in_dir(DWNLD_DIR)
    if downloaded_files_cnt != files_in_folder_cnt:
        logging.error(
            "Files count in folder doesn't match. Files in folder: " + str(
                files_in_folder_cnt) + ", downloaded files checksum: " + str(
                downloaded_files_cnt) + ".")
    else:
        logging.info("Files count check ok. Downloaded files: " + str(
            files_in_folder_cnt) + ".")

    if done_now_cnt != len(spc_list):
        logging.warning("Not all SPC in list are done! Done " + str(
            done_now_cnt) + " of " + str(len(spc_list)) + " SPCs.")
    else:
        logging.info(
            "All SPCs in list done. Done " + str(done_now_cnt) + " SPCs.")

    for key, value in count.items():
        if value != old_count[key]:
            logging.info("There was some changes in compare to old state.")
            break
    else:
        logging.info("No changes compare to old state.")

    counts = {}
    for key, value in count.items():
        counts[key] = (value, old_count[key])

    return counts


def write_csv(spc_list, filename):
    headers = ("kod_sukl", "naz_obch", "rc", "rok", "status", "nazev souboru",
               "stazeno", "stejne spc", "url")
    csv_file = ""
    csv_reader = ""
    while True:
        try:
            csv_file = open(filename, 'w', encoding="cp1250")
        except (OSError, PermissionError) as e:
            answer = query_retry_skip_abort(
                "Access to " + filename + " denied. Maybe opened in Excel - please close it.")
            if answer in ("break", "abort"):
                return
        else:
            break

    if csv_file:
        csv_reader = csv.writer(csv_file, dialect='excel2013')

    csv_reader.writerow(headers)
    for spc in spc_list:
        csv_reader.writerow((spc.sukl_code, spc.name, spc.reg_num, spc.year,
                             spc.status, spc.filename, spc.downloaded,
                             spc.duplicate, spc.url))
    csv_file.close()

    printc.info("Data written to " + filename + ".")
    return filename


def create_folder_structure():
    directories = [DWNLD_DIR, TXT_DIR, HTML_DIR, EMEA_DIR, OLD_DIR]
    for directory in directories:
        if not os.path.exists(directory):
            os.makedirs(directory)
        else:
            if not os.listdir(directory) == []:  # check if directory is empty
                answer = query_yes_no(
                    "WARNING: Directory \"" + directory + "\" is not empty. Clear?",
                    "no", True)
                if answer:
                    for file in os.listdir(directory):
                        file_path = os.path.join(directory, file)
                        if os.path.isfile(file_path):
                            os.remove(file_path)


def set_logger():
    formatter = ColoredFormatter("%(log_color)s%(levelname)-s: %(message)s",
                                 datefmt=None, reset=True,
                                 log_colors={'DEBUG': 'green', 'INFO': 'blue',
                                             'WARNING': 'yellow',
                                             'ERROR': 'red',
                                             'CRITICAL': 'black,bg_red', },
                                 secondary_log_colors={}, style='%')

    log_format = "%(levelname)s: %(message)s (%(funcName)s:%(lineno)d)"
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(logs)
    ch.setLevel(logging.INFO)
    # formatter = logging.Formatter(coloredFormatter)
    ch.setFormatter(formatter)
    root.addHandler(ch)

    return ch


def init_settings():
    logger = set_logger()

    # noinspection PyArgumentList
    csv.register_dialect('excel2013', delimiter=';', lineterminator='\n')

    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.WARNING)

    return logger


def print_log(clear=False):
    print(logs.getvalue())
    string = logs.getvalue()
    if clear:
        logs.seek(0)
        logs.truncate(0)


def extract_emea(spc_list):
    emea_dict = defaultdict(list)

    for spc in spc_list:
        if spc.status == status["emea"]:
            emea_dict.setdefault(spc.filename, []).append(spc)

    print("{:-<80}".format(""))
    print("Separating EMEA files.")
    print("{:-<80}".format(""))
    printc.info("Go to EMEA folder and open proper 'emea' file.")
    printc.info("Reg. number is automatically copied to clipboard.")
    printc.info(
        "Just find it in PDF, extract it and save it under new proper filename.")

    for emea_file in sorted(emea_dict):
        printc.cyan(
            "\n" + emea_file + " (" + str(len(emea_dict[emea_file])) + ")")
        for spc in emea_dict[emea_file]:
            filename = filename_from_spc_name(spc.name,
                                              os.path.splitext(spc.filename)[
                                                  1])
            print(spc.reg_num, end=": ")
            printc.green(filename, end="")
            pyperclip.copy(spc.reg_num)
            filesize = os.path.getsize(os.path.join(EMEA_DIR, filename))
            if filename_exists(EMEA_DIR, filename) and filesize != 0:
                print()
                continue
            else:
                input()

            while True:
                filesize = os.path.getsize(os.path.join(EMEA_DIR, filename))
                if not filename_exists(EMEA_DIR, filename) or filesize == 0:
                    answer = query_retry_skip_abort(
                        "ERROR: " + filename + " not good. Replace.")
                    if answer == "retry":
                        continue
                    elif answer == "skip":
                        break
                    else:
                        sys.exit(2)
                else:
                    spc.status = status["downloaded"]
                    spc.filename = filename
                    break


def move_files(src_dir, target_dir, extensions=[], incl_strs=[], excl_strs=[]):
    for file in os.listdir(src_dir):
        if not os.path.isfile(os.path.join(src_dir, file)):
            continue
        if extensions:
            file_ext = os.path.splitext(file)[1]
            for i in range(len(extensions)):
                if extensions[i][0] != ".":
                    extensions[i] = "." + extensions[i]
            if file_ext not in extensions:
                continue

        if incl_strs:
            for incl_str in incl_strs:
                if incl_str in file:
                    break
            else:
                continue

        if excl_strs:
            cont = False
            for excl_str in excl_strs:
                if excl_str in file:
                    cont = True
                    break
            if cont:
                continue

        os.rename(os.path.join(src_dir, file), os.path.join(target_dir, file))

        dir_name = os.path.splitext(file)[0] + "_files"
        if os.path.isdir(os.path.join(src_dir, dir_name)):
            os.rename(os.path.join(src_dir, dir_name), os.path.join(target_dir, dir_name))


def remove_duplicates(spc_list, duplicates):
    for spc in spc_list:
        printc.red(spc.filename)
        if spc.filename in duplicates.keys():
            printc.red("delete: " + spc.filename)
            printc.green("using: " + duplicates[spc.filename])
            print("---")
            # os.remove(os.path.join(path, spc.filename))
            basename = os.path.splitext(spc.filename)[0]
            os.remove(os.path.join(TXT_DIR, spc.filename))
            os.remove(os.path.join(DWNLD_DIR, basename + ".pdf"))
            os.remove(os.path.join(HTML_DIR, basename + ".html"))
            spc.filename = duplicates[spc.filename]
            spc.status = status["duplicate"]


def doc_to_txt(doc_dir, spc_list):
    printc.info("Converting DOC to TXT")
    if os.name != "posix":
        printc.error(
            "Sorry, can't do it automatically, antiword is only (cygwin)")
        printc.warning(
            "Open Word and save all DOC files in folder as TXT, with the same name, in the same folder.")
        input()
        for spc in spc_list:
            if spc.status == status["converted_to_txt"]:
                continue

            basename, extension = os.path.splitext(spc.filename)
            txt_filename = basename + ".txt"
            if extension != ".doc":
                continue
            if not os.path.isfile(
                    os.path.join(doc_dir, spc.filename)) and spc.status != \
                    status["old"]:
                printc.error(spc.filename + " doesn't exist.")
                continue
            while True:
                if not os.path.isfile(
                        os.path.join(doc_dir, txt_filename)) and spc.status != \
                        status["old"]:
                    answer = query_retry_skip_abort(
                        txt_filename + " doesn't exist.")
                    if answer == "retry":
                        continue
                    elif answer == "skip":
                        return
                    else:
                        sys.exit()
                else:
                    spc.status = status["converted_to_txt"]
                    break
    else:  # posix system, where antiword is installed
        for spc in spc_list:
            if spc.status == status["converted_to_txt"]:
                continue

            basename, extension = os.path.splitext(spc.filename)
            txt_filename = basename + ".txt"
            if extension != ".doc":
                continue
            if not os.path.exists(
                    os.path.join(doc_dir, spc.filename)) and spc.status != \
                    status["old"]:
                printc.error(spc.filename + " doesn't exist.")
                continue

            printc.green("\n" + spc.filename)
            cmd = "antiword -t " + os.path.join(doc_dir, spc.filename)
            ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT)
            text = ps.communicate()[0].decode("utf-8")
            print(txt_filename)
            txt_file = open(os.path.join(doc_dir, txt_filename), 'w',
                            encoding="utf-8")
            txt_file.write(text)
            txt_file.close()

            if not os.path.exists(
                    os.path.join(doc_dir, txt_filename)) and spc.status != \
                    status["old"]:
                printc.error(txt_filename + " doesn't exist.")
            else:
                spc.status = status["converted_to_txt"]


def open_unknown_encoding_file(file, print_it=True):
    if print_it:
        print(file)
    encodings = ['utf-8', 'windows-1250', 'windows-1252']
    for e in encodings:
        try:
            fh = open(file, 'r', encoding=e)
            text = fh.read()
            fh.close()

        except UnicodeDecodeError:
            # print('got unicode error with %s , trying different encoding' % e)
            pass
        else:
            # print('opening the file with encoding:  %s ' % e)
            return text
    else:
        return None


def strip_html_tags(html):
    txt = manipulate_html(html, "html_to_txt")
    return txt


def prettify(html):
    new_html = manipulate_html(html, "prettify")
    return new_html


def manipulate_html(html, command=""):
    soup = BeautifulSoup(html)
    new_ext = ""
    if command == "prettify":
        # print("Prettifying")
        modified_html = soup.prettify()
    elif command == "html_to_txt":
        # print("HTML to TXT")
        modified_html = soup.getText()
    else:
        printc.error("Bad command for manipulate HTML!")
        return

    return modified_html


def far_csv_to_dict(far_csv):
    csv_file = open(far_csv, 'r', encoding='cp1250')
    csv_reader = csv.reader(csv_file, dialect='excel2013')
    header = next(csv_reader)  # read only one line

    far_list = OrderedDict()
    for row in csv_reader:
        if row[0] == "":
            continue

        if row[0][0] == "#":
            section = row[0][1:].strip()
            far_list[section] = []
        else:
            far_list[section].append(row)

    csv_file.close()

    for section in far_list.keys():
        for row in far_list[section]:
            if row[2] == "i":
                row[0] = re.compile(row[0], flags=re.IGNORECASE)
            elif row[2] == "s":  # include newlines to universal dot character
                row[0] = re.compile(row[0], flags=re.MULTILINE | re.DOTALL)
            elif row[2] == "m":
                row[0] = re.compile(row[0], flags=re.MULTILINE)
            elif row[2] == "r" or row[2] == "c":
                continue
            else:
                row[0] = re.compile(row[0])

    return far_list


def find_and_replace_from_list(far_csv, html, sections=("all",)):
    global far_dict
    if not far_dict:
        far_dict = far_csv_to_dict(far_csv)

    # printc.yellow(str(sections))
    for section in far_dict:
        # printc.red(section)
        if section not in sections and "all" not in sections:
            continue
        for i, far_row in enumerate(far_dict[section]):
            if far_row[2] == "r":
                html = html.replace(far_row[0], far_row[1])
            else:
                html = far_row[0].sub(far_row[1], html)

    return html


def change_status_to_converted_to_html(spc_list):
    for spc in spc_list:
        if spc.status == status["downloaded"]:
            basename, extension = os.path.splitext(spc.filename)
            html_filename = basename + ".html"
            html_file_dir = basename + "_files"
            while True:
                if not os.path.isfile(os.path.join(DWNLD_DIR, html_filename)):
                    answer = query_retry_skip_abort(
                        html_filename + " with " + html_file_dir + " doesn't exist.")
                    if answer == "retry":
                        continue
                    elif answer == "skip":
                        return
                    else:
                        sys.exit()
                else:
                    spc.status = status["converted_to_html"]
                    spc.filename = html_filename
                    break


def convert_to_html_with_word():
    printc.info("Opening Word for converting PDF and DOC files to HTML...")
    printc.info("File path putted in clipboard, just paste it.")
    printc.info("Close Word when everything is converted.")
    word_path = ""
    if os.name == "posix":
        word_path = word_path_cygwin
    else:
        word_path = word_path_win
    if not os.path.isfile(word_path):
        printc.error("Word not found. Check path: " + word_path)

    abspath = os.path.abspath(DWNLD_DIR)

    # TODO fast bug fix
    abspath = abspath.replace("/cygdrive/c", "C:")
    pyperclip.copy(abspath)

    cmd = [word_path, "-m" + word_macro_name]
    subprocess.call(cmd)


def diff(file_a, file_b):
    diff_path = ""
    if os.name == "posix":
        diff_path = diff_path_cygwin
    else:
        diff_path = diff_path_win
    if not os.path.isfile(diff_path):
        printc.error("Diff program not found. Check path: " + diff_path)

    cmd = [diff_path, file_a, file_b]
    subprocess.call(cmd)


def sublime(file_a):
    sublime_path = ""
    if os.name == "posix":
        sublime_path = sublime_path_cygwin
    else:
        sublime_path = sublime_path_win
    if not os.path.isfile(sublime_path):
        printc.error("Diff program not found. Check path: " + sublime_path)

    cmd = [sublime_path, file_a]
    subprocess.call(cmd)


def get_section_indexes(txt):
    sections = ["<<K1>>", "<<K2>>", "<<K3>>", "<<K4>>", "<<K41>>", "<<K42>>",
                "<<K43>>", "<<K44>>", "<<K45>>", "<<K46>>", "<<K47>>",
                "<<K48>>", "<<K49>>", "<<K5>>", "<<K51>>", "<<K52>>",
                "<<K53>>", "<<K6>>", "<<K61>>", "<<K62>>", "<<K63>>",
                "<<K64>>", "<<K65>>", "<<K66>>", "<<K7>>", "<<K8>>", "<<K9>>",
                "<<K10>>"]
    section_indices = {}

    for i, section in enumerate(sections):
        index = re.findtiter(i, txt)
        section_begin = index.start()

        try:
            index = re.findtiter(i + 1, txt)
        except KeyError as e:
            section_end = len(txt)
        else:
            section_end = index.start()

        section_indices[section] = (section_begin, section_end)

    return section_indices


def html_table_to_txt(html_table, parameters={"type": "htmltab"}):
    big_comma = False
    table_array = []
    longest_strings = []
    colspan_left = 0
    rowspan_left = 0
    rowspan_indexes = []
    rowspan_height = 0
    colspan_width = 0

    if "big_comma" in parameters:
        big_comma = True

    soup = BeautifulSoup(html_table)
    table_html = soup.find("table")

    tr_all = table_html.find_all("tr")
    rows_num = len(tr_all)

    # iterate through every <tr> and find every <td> in it
    for row_it, tr in enumerate(tr_all):
        table_array.append([])

        td_all = tr.find_all("td")
        # tags = tr.select('td[rowspan,colspan]')
        # printc.blue(str(tags))

        # iterate throught every <td> and retrieve data from it
        for col_it, td in enumerate(td_all):
            param = {}
            if td.has_attr('colspan'):
                param['colspan'] = True
                param['colspan_width'] = int(td['colspan'])
                param['colspan_pos'] = "begin"
                colspan_left += int(td['colspan']) - 1
                colspan_width = int(td['colspan'])
                # print("colspaan", td.span['colspan'])
            if td.has_attr('rowspan'):
                param['rowspan'] = True
                param['rowspan_height'] = int(td['rowspan'])
                param['rowspan_pos'] = "begin"
                rowspan_left += int(td['rowspan']) - 1
                rowspan_height = int(td['rowspan'])
                # print("rowspaan", td.span['rowspan'])
            # print("td:", td)
            string = ''.join(td.find_all(text=True)).strip()
            # printc.blue(string)
            if "join_lines" in parameters:
                string = string.replace('\n', ' ')
            # printc.red(string)
            string = re.sub(" +", " ", string)

            if big_comma:
                string = re.sub("([^:,]) ([A-ZÚŠČŘŽ])", "\g<1>, \g<2>", string)

            # save value of longest string in column
            if len(longest_strings) < col_it:
                printc.error("ERROR: list is smaller than index")
            if len(longest_strings) == col_it:
                longest_strings.append(len(string))
            elif len(string) > longest_strings[col_it]:
                longest_strings[col_it] = len(string)

            row = len(table_array) - 1
            col = len(table_array[-1])
            if (row, col) in rowspan_indexes:
                rowspan_param = {'rowspan': True}
                if not rowspan_height:
                    printc.error("Rowspan width not defined")
                rowspan_param['rowspan_height'] = rowspan_height
                rowspan_indexes.remove((row, col))
                if not rowspan_indexes:
                    rowspan_height = 0
                    rowspan_param['rowspan_pos'] = "end"
                else:
                    rowspan_param['rowspan_pos'] = "middle"

                table_array[-1].append(
                    {"string": "", "parameters": rowspan_param})
            # append cell data to whole table array
            table_array[-1].append({"string": string, "parameters": param})

            # append empty cells where rowspan is
            while colspan_left > 0:
                colspan_param = {'colspan': True}
                if not colspan_width:
                    printc.error("Colspan width not defined")
                colspan_param['colspan_width'] = colspan_width
                colspan_left -= 1
                if not colspan_left:
                    colspan_param['colspan_pos'] = "end"
                else:
                    colspan_param['colspan_pos'] = "middle"
                table_array[-1].append(
                    {"string": "", "parameters": colspan_param})

            if rowspan_left:
                # index of rowspan in table
                row = len(table_array) - 1
                col = len(table_array[-1]) - 1
                while rowspan_left > 0:
                    row += 1
                    rowspan_indexes.append((row, col))
                    rowspan_left -= 1

    txt = array_table_to_txt(table_array, parameters)
    return txt


def clean_table_array(table_array):
    # delete blank columns from end
    max_columns = 0
    for i in range(len(table_array)):
        first_blank_col = len(table_array[i])
        # print(first_blank_col)
        for j in range(len(table_array[i]))[::-1]:
            # print("[{}][{}] {}".format(i, j, table_array[i][j]))
            if re.match("^\s*$", table_array[i][j]):
                first_blank_col = j
                # print(first_blank_col, "changed")
            else:
                break
        table_array[i] = table_array[i][:first_blank_col]
        if first_blank_col > max_columns:
            max_columns = first_blank_col

    # filter empty rows
    table_array = [x for x in table_array if x]

    # make all rows have same number of cols
    for i in range(len(table_array)):
        if len(table_array[i]) > max_columns:
            printc.error("Max columns are counted badly.")
        while len(table_array[i]) < max_columns:
            table_array[i].append("")

    # remove empty columns
    empty_columns = []
    for j in range(max_columns):
        for i in range(len(table_array)):
            # print("[{}][{}] {}".format(i, j, table_array[i][j]))
            if not re.match("^\s*$", table_array[i][j]):
                break
        else:
            empty_columns.append(j)
            # for ib in range(len(table_array)):
            #     print("[{}][{}] {}".format(ib, j, table_array[ib][j]))
    if empty_columns:
        # print("empty columns:")
        # [print(empty) for empty in empty_columns]
        for i in range(len(table_array)):
            row = []
            for j in range(len(table_array[i])):
                if j in empty_columns:
                    continue
                else:
                    row.append(table_array[i][j])
            table_array[i] = row

    # print("max columns", max_columns)
    # for i in range(len(table_array)):
    #     for j in range(len(table_array[i])):
    #         print("[{}][{}] {}".format(i, j, table_array[i][j]))
    # print("-")

    return table_array


def clean_cetnosti(string):
    if re.search("Velmi časté", string, re.I):
        string = "Velmi časté"
    elif re.search("Méně časté", string, re.I):
        string = "Méně časté"
    elif re.search("Časté", string, re.I):
        string = "Časté"
    elif re.search("Velmi vzácné", string, re.I):
        string = "Velmi vzácné"
    elif re.search("Vzácné", string, re.I):
        string = "Vzácné"
    elif re.search("Není známo", string, re.I):
        string = "Není známo"
    elif re.search("Četnost neznámá", string, re.I):
        string = "Není známo"
    else:
        pass
    return string


def nutab_top_header(table_array):
    cetnosti = []

    # make array of cetnosti
    i = 0
    for j in range(1, len(table_array[i])):
        # print("[" + str(i) + "][" + str(j) + "] " + table_array[i][j])
        cetnost_str = table_array[i][j]["string"]
        cetnost_str = clean_cetnosti(cetnost_str)
        cetnosti.append(cetnost_str)

    # workaround when table is splitted by page break (if there is no left column, join together)
    # TODO this has bug which can not handle rowspans and colspans and leads to IndexError, so it's disabled for now
    '''
    remove_indexes = set()
    for i in range(1, len(table_array)):
        if table_array[i][0] == "":
            for j in range(1, len(table_array[i])):
                string = table_array[i - 1][j] + " " + table_array[i][j]
                if "big_comma" in parameters:
                    string = re.sub("([^:,]) ([A-ZÚŠČŘŽ])", "\g<1>, \g<2>", string)
                table_array[i - 1][j] = string
            remove_indexes.add(i)
    for index in remove_indexes:
        del (table_array[index])
    '''
    table_line = "{0:-<{1}}".format("", nu_width)
    # table_str = table_line
    new_array = [table_line]

    for i in range(1, len(table_array)):
        new_array.append(table_array[i][0]["string"])
        # table_str += (table_array[i][0] + "\n")
        for j in range(1, len(table_array[i])):
            if table_array[i][j]["string"] != "":
                # table_str += (cetnosti[j - 1] + ": " + table_array[i][j] + "\n")
                new_array.append(
                    cetnosti[j - 1] + ": " + table_array[i][j]["string"])
        if i != len(table_array) - 1:
            # table_str += "-\n"
            new_array.append("-")

    new_array.append(table_line)
    new_array.append("")
    # table_str += table_line
    table_str = "\n".join(new_array)

    return table_str


def nutab_left_header_2col(table_array):
    table_line = "{0:-<{1}}".format("", nu_width)
    delimiter_char = '-'
    delimiter = '\n' + delimiter_char + '\n'
    new_array = []
    for row in table_array:
        new_array.append('\n'.join((row[0]["string"], row[1]["string"])))
    new_array = delimiter.join(new_array)

    table_str = '\n'.join((table_line, new_array, table_line, ""))
    return table_str


def nutab_left_header_3col(table_array):
    table_line = "{0:-<{1}}".format("", nu_width)
    delimiter_char = '-'
    delimiter = '\n' + delimiter_char + '\n'

    new_array = []
    row = []

    for table_row in table_array:
        if table_row[0]["string"]:
            if row:
                new_array.append('\n'.join(row))
                row = []
            row.append(table_row[0]["string"])
        row.append(table_row[1]["string"] + ": " + table_row[2]["string"])

    table_str = delimiter.join(new_array)
    table_str = '\n'.join((table_line, table_str, table_line, ""))
    return table_str


def nutab_top_header_sections_odd(table_array):
    # TODO not properly functioning
    table_line = "{0:-<{1}}".format("", nu_width)
    delimiter_char = '-'
    delimiter = '\n' + delimiter_char + '\n'
    cetnosti = []

    # make array of cetnosti
    for j in range(1, len(table_array[0])):
        # print("[" + str(i) + "][" + str(j) + "] " + table_array[i][j])
        cetnost_str = table_array[0][j]["string"]
        cetnost_str = clean_cetnosti(cetnost_str)
        cetnosti.append(cetnost_str)

    new_array = []
    row = []
    header = True
    for i in range(len(table_array))[1:]:
        for j in range(len(table_array[i])):
            if header:
                row.append(table_array[i][j]["string"])
                break
            else:
                if table_array[i][j]["string"] != "":
                    row.append(
                        cetnosti[j - 1] + ": " + table_array[i][j]["string"])
        if not header:
            new_array.append('\n'.join(row))
            row = []
        header = not header

    table_str = delimiter.join(new_array)
    table_str = '\n'.join((table_line, table_str, table_line, ""))

    return table_str


def nutab_left_header_more_tables(table_array):
    table_line = "{0:-<{1}}".format("", nu_width)
    delimiter_char = '-'
    delimiter = '\n' + delimiter_char + '\n'
    headers = []
    final_multiple_table_str = ""
    for header in table_array[0][2:]:
        headers.append(header["string"])
    printc.red(str(headers))

    for i in range(len(headers)):
        new_array = []
        row = []

        for table_row in table_array[1:]:
            if table_row[0]["string"]:
                if row:
                    new_array.append('\n'.join(row))
                    row = []
                row.append(table_row[0]["string"])
            if table_row[2 + i]["string"]:
                row.append(
                    table_row[1]["string"] + ": " + table_row[2 + i]["string"])

        table_str = delimiter.join(new_array)
        table_str = '\n'.join((table_line, table_str, table_line, ""))
        final_multiple_table_str += ('\n'.join(("-", headers[i], table_str)))

    # TODO remove empty sections
    return final_multiple_table_str


def nutab_left_header_more_tables_freq_right(table_array):
    table_line = "{0:-<{1}}".format("", nu_width)
    delimiter_char = '-'
    delimiter = '\n' + delimiter_char + '\n'
    headers = []
    final_multiple_table_str = ""
    for header in table_array[0][2:]:
        headers.append(header["string"])
    printc.red(str(headers))

    for i in range(len(headers)):
        new_array = []
        row = []

        for table_row in table_array[1:]:
            if table_row[0]["string"]:
                if row:
                    new_array.append('\n'.join(row))
                    row = []
                printc.yellow(table_row[0]["string"])
                row.append(table_row[0]["string"])
            try:
                if re.match("[^ *-? *]", table_row[2 + i]["string"]):
                    printc.blue(
                        table_row[2 + i]["string"] + ": " + table_row[1][
                            "string"])
                    row.append(
                        table_row[2 + i]["string"] + ": " + table_row[1][
                            "string"])
            except IndexError as e:
                print(e)

        table_str = delimiter.join(new_array)
        table_str = '\n'.join((table_line, table_str, table_line, ""))
        final_multiple_table_str += ('\n'.join(("-", headers[i], table_str)))

    # TODO remove empty sections
    return final_multiple_table_str


def array_table_to_txt(table_array, parameters):
    far_csv = os.path.join(os.path.split(sys.argv[0])[0], "far.csv")

    # find and replace in tables too
    for row_it in range(len(table_array)):
        for col_it in range(len(table_array[row_it])):
            string = table_array[row_it][col_it]["string"]
            string = find_and_replace_from_list(far_csv, string, (
                "whitespaces", "txt-chars", "experimental", "typography"))
            string = replace_viz_body(string, far_csv)
            table_array[row_it][col_it]["string"] = string

    if parameters["type"] == "htmltab":
        beautiful_table = BeautifulTable(table_array, parameters=parameters)
        table_str = beautiful_table.convert_to_string()
    elif parameters["type"] == "nutab_top_header":
        table_str = nutab_top_header(table_array)
    elif parameters["type"] == "nutab_left_header_2col":
        table_str = nutab_left_header_2col(table_array)
    elif parameters["type"] == "nutab_left_header_3col":
        table_str = nutab_left_header_3col(table_array)
    elif parameters["type"] == "nutab_top_header_sections_odd":
        table_str = nutab_top_header_sections_odd(table_array)
    elif parameters["type"] == "nutab_left_header_more_tables":
        table_str = nutab_left_header_more_tables(table_array)
    elif parameters["type"] == "nutab_left_header_more_tables_freq_right":
        table_str = nutab_left_header_more_tables_freq_right(table_array)



    # remove trailing spaces in table
    table_str = re.sub(" +\n", "\n", table_str)

    return table_str


def process_all_tables(html):
    all_tables = []
    table_it = 0

    soup = BeautifulSoup(html)
    tables_html = soup.find_all("table")
    # printc.cyan(str(html))
    # printc.magenta(str(soup))
    # printc.yellow(str(tables_html))
    # iterate through every table in html document
    for table_html in tables_html:

        txt_table = html_table_to_txt(str(table_html))
        if txt_table is None:
            table.str = clean_html(table.html, far_csv_glob)
        table = Table()
        table.html = str(table_html)
        table.str = txt_table
        table.num = table_it
        all_tables.append(table)
        table_it += 1

    # get indexes of begin/end of html chunks without tables
    non_table_indexes = []
    h_begin = 0
    h_end = 0
    for t_begin, t_end in table_indexes(html):
        h_end = t_begin
        non_table_indexes.append((h_begin, h_end))
        h_begin = t_end
    non_table_indexes.append((h_begin, len(html)))

    # make modified html without tables, but with commented placeholders
    new_html = ""
    table_it = 0
    for h_begin, h_end in non_table_indexes[:len(non_table_indexes) - 1]:
        new_html = new_html + html[h_begin:h_end]
        new_html = new_html + table_begin_str + str(table_it)
        new_html = new_html + "\n" + table_end_str
        table_it += 1
    h_begin, h_end = non_table_indexes[-1]
    new_html = new_html + html[h_begin:h_end]

    # return array with all tables and html content without tables
    return all_tables, new_html


def table_indexes(html):
    table_begins = []
    table_ends = []
    for i in re.finditer("<table", html):
        table_begins.append(i.start())

    for i in re.finditer("</table>", html):
        table_ends.append(i.end())

    table_indices = zip(table_begins, table_ends)

    return table_indices


def parse_parameters(regex):
    parameters = {"type": "htmltab"}

    if regex:
        if " raw" in regex:
            parameters["raw"] = True
        if " from_raw" in regex:
            parameters["from_raw"] = True
        if " big_comma" in regex:
            parameters["big_comma"] = True
        if " nutab_top_header" in regex:
            parameters["type"] = "nutab_top_header"
        if " nutab_left_header_2col" in regex:
            parameters["type"] = "nutab_left_header_2col"
        if " nutab_left_header_3col" in regex:
            parameters["type"] = "nutab_left_header_3col"
        if " nutab_top_header_sections_odd" in regex:
            parameters["type"] = "nutab_top_header_sections_odd"
        if " nutab_left_header_more_tables" in regex:
            parameters["type"] = "nutab_left_header_more_tables"
        if " nutab_left_header_more_tables_freq_right" in regex:
            parameters["type"] = "nutab_left_header_more_tables_freq_right"
        if " line_after" in regex:
            match = re.search(" line_after ?\(([\d, ]+)\)", regex).group(1)
            lines = re.findall('\d+', match)
            lines = list(map(int, lines))
            parameters["line_after"] = lines
        if " wrap_widths" in regex:
            match = re.search(" wrap_widths ?\(([\d, ]+)\)", regex).group(1)
            widths = re.findall('\d+', match)
            widths = list(map(int, widths))
            parameters["wrap_widths"] = widths
        if " wrap_before" in regex:
            match = re.search(" wrap_before ?\(([^\)]+)\)", regex).group(1)
            patterns = match.split(',')
            if "lpar" in patterns:
                index = patterns.index("lpar")
                patterns[index] = '\('
            if "rpar" in patterns:
                index = patterns.index("rpar")
                patterns[index] = '\)'
            if "comma" in patterns:
                index = patterns.index("comma")
                patterns[index] = ','
            parameters["wrap_before"] = patterns
        if " wrap_after" in regex:
            match = re.search(" wrap_after ?\(([^\)]+)\)", regex).group(1)
            patterns = match.split(',')
            if "lpar" in patterns:
                index = patterns.index("lpar")
                patterns[index] = '\('
            if "rpar" in patterns:
                index = patterns.index("rpar")
                patterns[index] = '\)'
            if "comma" in patterns:
                index = patterns.index("comma")
                patterns[index] = ','
            parameters["wrap_after"] = patterns
        if " hyphens" in regex:
            parameters["hyphens"] = True
        if " join_lines" in regex:
            parameters["join_lines"] = True
    return parameters


def find_table_positions(text, leave_table_line=True):
    tables = {}
    # print(len(table_end_str + '.*'))
    # for i in re.finditer(table_begin_str + '.*', text):
    for i in re.finditer(table_begin_str + '.*', text):
        table = Table(parameters=None)
        if leave_table_line:
            table.start = i.end() + 1
        else:
            table.start = i.start()
        table.regex = text[i.start():i.end()]
        table.start_len = len(i.group())
        regex = table.regex
        offset = len(table_begin_str)
        regex = regex[offset:]
        table_num = re.match('\d+', regex).group()
        num = int(table_num)
        table.num = num
        table.len = len(table.str)
        regex = regex[len(table_num):]
        table.parameters = parse_parameters(regex)

        tables[num] = table

    keys = list(tables.keys())

    i = 0
    for match in re.finditer('\n' + table_end_str, text):
        index = keys[i]
        tables[index].end = match.end() - len(table_end_str)
        tables[index].end_len = len(table_end_str)
        i += 1

    return tables


def html_table_to_nonformatted_text(html_table, parameters={}):
    big_comma = False
    table_str = ""
    table_array = []
    longest_strings = []

    # print(html_table)
    soup = BeautifulSoup(html_table)
    table_html = soup.find("table")

    tr_all = table_html.find_all("tr")
    rows_num = len(tr_all)

    # iterate through every <tr> and find every <td> in it
    for row_it, tr in enumerate(tr_all):
        table_array.append([])
        td_all = tr.find_all("td")
        cols_num = len(td_all)

        # iterate throught every <td> and retrieve data from it
        for col_it, td in enumerate(td_all):
            string = ''.join(td.find_all(text=True)).strip().replace('\n', ' ')
            string = re.sub(" +", " ", string)

            if big_comma:
                string = re.sub("([^:,]) ([A-ZÚŠČŘŽ])", "\g<1>, \g<2>", string)

            # append cell data to whole table array
            table_array[-1].append(string)

    rows = []
    for i, row in enumerate(table_array):
        for j, col in enumerate(row):
            table_array[i][j] = col_char + col

    for row in table_array:
        rows.append(''.join(row))
    # table_str = '\n'.join(table_str_row_array)
    table_str = row_char.join(rows)
    table_str = begin_boundaries_string + table_str + end_boundaries_string
    # print(table_str)
    return table_str


def nonformatted_table_to_table_array(txt_table):
    begin = re.search(begin_boundaries_string, txt_table).end()
    end = len(txt_table)
    for i in re.finditer(end_boundaries_string, txt_table):
        end = i.start()

    txt_table = txt_table[begin:end]

    rows = re.split(row_char, txt_table)

    table_array = []
    for row in rows:
        cols = re.split(col_char, row)
        table_array.append(cols[1:])

    return table_array


def reinsert_table(txt, html):
    html = clean_html(html, far_csv_glob)
    html = find_and_replace_from_list(far_csv_glob, html, ("html",))
    tables = find_table_positions(txt)
    soup = BeautifulSoup(html)
    html_tables = soup.find_all("table")

    # for table in tables_pos:

    for i, html_table in enumerate(html_tables):
        if i in tables.keys():
            tables[i].html = str(html_table)
            print(tables[i].start, tables[i].end)

    for table in tables.values():
        if "raw" in table.parameters:
            table.str = html_table_to_nonformatted_text(table.html,
                                                        table.parameters)
        elif "from_raw" in table.parameters:
            table_array = nonformatted_table_to_table_array(
                txt[table.start:table.end])
            table.str = array_table_to_txt(table_array, table.parameters)
        else:
            table.str = html_table_to_txt(table.html, table.parameters)
            if table.str is None:
                table.str = clean_html(table.html, far_csv_glob)
        table.len = len(table.str)
        table.offset = len(table.str) - (table.end - table.start)

    offset = 0

    for table in tables.values():
        # print("'" + table.str + "'")
        txt = txt[:table.start + offset] + table.str + txt[table.end + offset:]
        offset += table.offset

    return txt


def check_chapters(txt):
    global CHAPTER_ERROR
    chapters = ["<<K0>>", "<<K1>>", "<<K2>>", "<<K3>>", "<<K4>>", "<<K41>>",
                "<<K42>>", "<<K43>>", "<<K44>>", "<<K45>>", "<<K46>>",
                "<<K47>>", "<<K48>>", "<<K49>>", "<<K5>>", "<<K51>>",
                "<<K52>>", "<<K53>>", "<<K6>>", "<<K61>>", "<<K62>>",
                "<<K63>>", "<<K64>>", "<<K65>>", "<<K66>>", "<<K7>>", "<<K8>>",
                "<<K9>>", "<<K10>>"]

    my_map = {}
    for chapter in chapters:
        my_map[chapter] = 0

    for chapter in chapters:
        for found in re.finditer(chapter, txt):
            my_map[chapter] += 1

    ok = True
    for chapter, cnt in sorted(my_map.items()):
        if cnt > 1:
            CHAPTER_ERROR += 1
            print(CHAPTER_ERROR, chapter + ": multiple times!")
            ok = False
        if cnt < 1:
            print(CHAPTER_ERROR, chapter + ": missing!")
            CHAPTER_ERROR += 1
            ok = False

    return ok


def insert_tables(tables, txt):
    old_txt = txt
    leave_table_line = True
    tables_pos = find_table_positions(txt, leave_table_line)

    for i in re.finditer("<<K48>>.*\n", txt):
        nu_section_begin = i.end()

    for i in re.finditer("\n<<K49>>", txt):
        nu_section_end = i.start()

    if len(tables_pos) != len(tables):
        printc.error(
            "Table space in file does not correspond with saved tables.")
        return old_txt
    try:
        offset = 0

        for i, table_pos in tables_pos.items():

            begin = table_pos.start + offset
            if leave_table_line:
                end = begin
            else:
                end = table_pos.end
            # print(begin, end)
            txt = txt[:begin] + tables[i].str + txt[end:]
            if leave_table_line:
                offset += len(tables[i].str)
            else:
                # TODO this offset doesn't work
                offset += len(tables[
                                  i].str) - table_pos.start_len - table_pos.end_len * 2
    except Exception as e:
        printc.error(str(e))
        return old_txt

    return txt


def save_as(data, filename, path="", suffix="", extension="",
            encoding="utf-8"):
    if not path:
        path = os.path.split(filename)[0]

    if path and not os.path.isdir(path):
        os.mkdir(path)

    filename = os.path.split(filename)[1]
    basename = os.path.splitext(filename)[0]

    if not extension:
        extension = os.path.splitext(filename)[1]
    else:
        if extension[0] != ".":
            extension = "." + extension

    new_filename = os.path.join(path, basename) + suffix + extension
    # print("save as: " + new_filename + " with encoding: " + encoding)

    file = open(new_filename, 'w', encoding=encoding)
    file.write(data)
    file.close()

    return new_filename


def remove_html_attrs(html):
    soup = BeautifulSoup(html)
    for tag in soup.findAll(True):
        tag.attrs = {}
    return str(soup)


def wrap_paragraph(text, width, hyphenator, hyphen='-'):
    text_array = text.split(' ')
    final_array = []
    row = []
    row_len = 0
    # print(text_array)
    # width = 3
    # print("width", width)
    # TODO this is a real mess..Do sth about it!!!
    for w, word in enumerate(text_array):
        if word[:1] == "\n":
            if row:
                final_array.append(' '.join(row))
            row.clear()
            row_len = 0
            word = word[1:]
        if row_len + len(word) + len(row) <= width:  # if it fits to one row
            row.append(word)
            row_len += len(word)
        else:  # if it doesn't fit to one row
            if row_len + len(row) > math.floor(width / 3):
                final_array.append(' '.join(row))
                row.clear()
                row_len = 0
                text_array.insert(w + 1, word)
                continue
            hyph_pos = hyphenator.positions(word)
            for i in reversed(hyph_pos):  # take it from longest
                index = i
                if row_len + len(row) + index + len(hyphen) > width:
                    continue
                else:
                    break
            else:  # if it cannot be broken
                if hyph_pos and len(word) <= width:
                    final_array.append(' '.join(row))
                    row.clear()
                    row_len = 0
                    text_array.insert(w + 1, word)
                    continue
                elif len(word) > width:
                    while True:
                        free_chars = width - (row_len + len(row) + len(hyphen))
                        if free_chars < 1:
                            final_array.append(' '.join(row))
                            row.clear()
                            row_len = 0
                        else:
                            break
                    row.append(word[:free_chars] + hyphen)
                    final_array.append(' '.join(row))
                    row.clear()
                    row_len = 0
                    text_array.insert(w + 1, word[free_chars:])
                    continue
                else:
                    if row:
                        final_array.append(' '.join(row))
                    row.clear()
                    row_len = 0
                    row.append(word)
                    row_len += len(word)
                    continue

            row.append(word[:index] + hyphen)
            final_array.append(' '.join(row))
            row.clear()
            row_len = 0
            text_array.insert(w + 1, word[index:])
            # row_len += len(word[index:])

    final_array.append(' '.join(row))
    # print(final_array)
    longest_row = 0
    for row in final_array:
        if len(row) > longest_row:
            longest_row = len(row)

    final_paragraph = '\n'.join(final_array)
    # print("---------")
    # print(final_paragraph)
    # print("---------")

    return longest_row, final_paragraph


def file_position_of_match(txt, match):
    print(match.group())
    pos = match.start()
    txt_array = txt.split('\n')
    chars = 0
    for i, row in enumerate(txt_array):
        chars += len(row) + 1
        if chars < pos:
            continue
        else:
            break
    my_col = len(row) - (chars - pos - 1) + 1
    my_row = i + 1
    return my_row, my_col


def check_final_spc(txt):
    match = re.search(
        "[^A-Za-zěščřžýáíéúůóďňťÓÚŮĚĎŤŠČŘŽÝÁÍÉ\d\n <>\-\(\)/=,\.\*:%$\"\+;\[\]&\^'!\|]",
        txt)
    # print(match)
    if match:
        return file_position_of_match(txt, match)
    else:
        return None, None


def multiline_grep(txt, pattern, surrounding_len=40):
    blue = '\x1b[34m'
    white = '\x1b[37m'
    red = '\x1b[31m'
    reset = '\x1b[0m'

    txt = txt.replace("\n", "\\n")
    matches = re.finditer(pattern, txt)
    for i, match in enumerate(matches):
        begin_all = match.start() - surrounding_len
        end_all = match.end() + surrounding_len
        string = txt[begin_all:end_all]
        # string = string.replace("\n", "\\n")
        begin = surrounding_len
        end = len(string) - surrounding_len
        if begin > 0 and end < len(txt):
            if i % 2 == 0:
                print(blue + str(i) + " ..." + string[:begin] + red + string[
                                                                      begin:end] + blue + string[
                                                                                          end:] + "..." + reset)
            else:
                print(white + str(i) + " ..." + string[:begin] + red + string[
                                                                       begin:end] + white + string[
                                                                                            end:] + "..." + reset)


def clean_html(html, far_csv):
    html = re.sub("\n", " ", html)
    html = re.sub("<!--.*?-->", " ", html)
    html = find_and_replace_from_list(far_csv, html, ("html-chars",))
    soup = BeautifulSoup(html)
    for match in soup.find_all("span"):
        match.unwrap()
    html = str(soup)

    html = find_and_replace_from_list(far_csv, html, ("html-chars",))
    # html = remove_html_attrs(html)

    return html


def replace_viz_body(txt, far_csv):
    global far_dict
    if not far_dict:
        far_dict = far_csv_to_dict(far_csv)
    section = "chapter-names"
    txt_array = []
    last_begin = 0
    last_end = 0
    section_numbers = []

    for far_row in far_dict[section]:
        section_numbers.append('"?' + far_row[1] + '"?')

    # TODO maybe its better to take regex names, not proper ones
    section_regex = "(" + "|".join(section_numbers) + ")?"
    ws = " ?\n? ?"
    # print(section_regex)
    # sys.exit()
    whole_regex = (
        "(bod(y|u|ě|ech)?|část( |i|ech)|viz(\.| )|oddíl(y|u|e|ech)?|odstav(ec|ce|cích)|kapitol(e|ách|y|a)|kap|sekc(e|ích))" + "\.?" + ws + section_regex + ws + "\d\.\d\.?\d?" + ws + section_regex + "( ?\n?(,|a|, a|nebo) ?" + ws + section_regex + ws + "\n?\d\.\d\.?\d?" + ws + section_regex + ws + ")*")
    # print(whole_regex)
    matches = re.finditer(whole_regex, txt, re.IGNORECASE)
    if not matches:
        # printc.blue("Nothing to replace")
        return txt

    for match in matches:
        begin = match.start()
        end = match.end()
        txt_array.append(txt[last_end:begin])
        string = match.group()
        string = string.replace("\n", " ")
        # printc.blue(str(begin) + " " + string)
        string = re.sub("(\d\.\d)\.\d", "\g<1>", string)
        # print(begin, string)
        for far_row in far_dict[section]:
            string = string.replace(far_row[0], '"' + far_row[1] + '"')
        for far_row in far_dict["regex-to-normal-chapter-names"]:
            string = re.sub('"?' + far_row[0] + '"? ?"?' + far_row[0] + '"?',
                            '"' + far_row[1] + '"', string, flags=re.I)
        # print(string)
        txt_array.append(string)
        last_begin = begin
        last_end = end
    txt_array.append(txt[last_end:])

    txt = "".join(txt_array)
    # print(txt)
    return txt


def html_to_txt(html):
    html = clean_html(html, far_csv_glob)
    html = find_and_replace_from_list(far_csv_glob, html, ("html",))
    tables, html = process_all_tables(html)

    soup = BeautifulSoup(html)
    for match in soup.find_all("p"):
        match.unwrap()
    html = str(soup)
    html = find_and_replace_from_list(far_csv_glob, html, "test-html")

    # save_as(html, file, suffix="-clean", path="!clean")
    txt = strip_html_tags(html)
    txt = find_and_replace_from_list(far_csv_glob, txt, (
        "whitespaces", "typography", "txt-chars"))
    # if not args.noviz:
    txt = replace_viz_body(txt, far_csv_glob)
    # if not args.nochapter:
    txt = find_and_replace_from_list(far_csv_glob, txt,
                                     ("chapters", "experimental"))
    # txt = find_and_replace_from_csv(far_csv_glob, txt, ("whitespaces", "typography", "txt-chars", "experimental", "viz-body"))
    txt = insert_tables(tables, txt)
    txt += "\n\n"

    return txt


def make_pairs_of_meds(path, split_char='-'):
    name_groups = {}
    for file in os.listdir(path):
        if not os.path.isfile(os.path.join(path, file)):
            continue
        # print(file)
        main_name = file.split(split_char)[0]
        if main_name not in name_groups:
            name_groups[main_name] = []
        name_groups[main_name].append(file)

    similar_pairs = []
    for key, value in name_groups.items():
        if len(value) <= 1:
            continue

        pairs = list(itertools.combinations(value, 2))
        for pair in pairs:
            similar_pairs.append(pair)

    return similar_pairs


def find_similar(path):
    similar_pairs = make_pairs_of_meds(path)

    for pair in similar_pairs:
        txt1 = open_unknown_encoding_file(os.path.join(path, pair[0]))
        txt2 = open_unknown_encoding_file(os.path.join(path, pair[1]))
        similarity = similar(txt1, txt2)
        printc.yellow(pair)
        printc.red(similarity)

        # if similarity > 0.99:


def get_chapter_text(txt, chapter_num, file):
    while not check_chapters(txt):
        sublime(file)
        input()
        txt = open_unknown_encoding_file(file)
    chapter_num = chapter_num.replace(".", "")
    chapter_char = "<<K" + chapter_num + ">>"
    match = re.search(chapter_char + ".*", txt)
    while not match:
        printc.error("Chapter", chapter_char, "not found.")
        return ""
    begin_pos = match.end() + 1

    if chapter_num == "10":
        return txt[begin_pos:]

    # printc.blue(part_of_txt)
    part_of_txt = txt[begin_pos:]
    match = re.search("<<K", part_of_txt)
    if not match:
        printc.error("Next chapter after", chapter_char, "not found.")
        return ""
    end_pos = match.start() - 1
    # print(end_pos.start())

    return part_of_txt[:end_pos]


def do_it_all(xls_filename):
    # xls_filename = rename_file_to_folder_name(xls_filename)
    csv_filename = xls_to_csv(xls_filename)
    create_folder_structure()
    spc_list = read_csv(csv_filename)
    try:
        download_spc_from_list(spc_list)
        count = check_sum(spc_list)
        print_footer(count)
        print_log()
        write_csv(spc_list, csv_filename)
        extract_emea(spc_list)
        move_files(EMEA_DIR, DWNLD_DIR, excl_strs=["[emea]"])
        write_csv(spc_list, csv_filename)
        convert_to_html_with_word()
        change_status_to_converted_to_html(spc_list)
        move_files(DWNLD_DIR, HTML_DIR, extensions=["html"])
        write_csv(spc_list, csv_filename)
        for spc in spc_list:
            if not spc.status == status["converted_to_html"]:
                continue
            file = spc.filename
            html = open_unknown_encoding_file(os.path.join(HTML_DIR, file))
            txt = html_to_txt(html)
            spc.status = status["converted_to_txt"]
            spc.filename = os.path.splitext(spc.filename)[0] + ".txt"
            save_as(txt, file, TXT_DIR, extension="txt")

        duplicates_list = find_duplicates(TXT_DIR)
        remove_duplicates(spc_list, duplicates_list)
        write_csv(spc_list, csv_filename)
        pairs = make_pairs_of_meds(TXT_DIR)
        find_files_with_duplicate_reg_codes(pairs, TXT_DIR, spc_list)
        find_old_files(spc_list, TXT_DIR)
        write_csv(spc_list, csv_filename)
    except Exception as e:
        printc.warning("\n" + str(e))
    write_csv(spc_list, csv_filename)


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def extract_reg_numbers(txt):
    txt = txt.replace('\n', ' ')
    txt = re.sub(" ?([/-]) ?", "\g<1>", txt)

    reg_num_match = re.findall("\d{2}/\d{3,4}/\d{2}-[A-Z]/?[A-Z]?", txt)
    if not reg_num_match:  # SUKL reg num check
        reg_num_match = re.findall("EU/\d/\d{2}/\d{3}/\d{3}", txt)

    return reg_num_match


def find_spc_in_list(spc_list, filename):
    for spc in spc_list:
        if spc.filename == filename:
            return spc
    printc.error("SPC", filename, "not found")
    return None


def find_files_with_duplicate_reg_codes(pairs, path, spc_list):
    for pair in pairs:
        file_a = os.path.join(path, pair[0])
        file_b = os.path.join(path, pair[1])
        txt_a = open_unknown_encoding_file(file_a, False)
        txt_b = open_unknown_encoding_file(file_b, False)
        chapter_a = get_chapter_text(txt_a, "8", file_a)
        chapter_b = get_chapter_text(txt_b, "8", file_b)
        # printc.cyan(chapter_a)
        reg_numbers_a = extract_reg_numbers(chapter_a)
        reg_numbers_b = extract_reg_numbers(chapter_b)

        if reg_numbers_a == reg_numbers_b:
            printc.green("A:", file_a)
            printc.green("B:", file_b)
            cmd = [diff_path_cygwin, file_a, file_b]
            subprocess.call(cmd)
            answer = query_a_b("Same reg. numbers in two files. Which file do you want to remove?")
            if answer == "a":
                chosen_file = file_a
            else:
                chosen_file = file_b

            os.remove(chosen_file)
            printc.error("Removing", chosen_file)
            spc = find_spc_in_list(spc_list, os.path.split(chosen_file)[1])
            spc.status = status["duplicate"]
            # printc.yellow("Changing status to duplicate in", spc)


        # print("---------------")
        # extract_reg_numbers(txtB)


def extract_dates(dates_str):
    # remove links which Word is putting there
    dates_str = re.sub("\n\[.*", "", dates_str)
    matches = re.findall("(\d{4})", dates_str)
    return matches


def move_old_files(filename):
    if not os.path.isdir(os.path.join(TXT_DIR, "old")):
        os.mkdir(os.path.join(TXT_DIR, "old"))
    if not os.path.isdir(os.path.join(HTML_DIR, "old")):
        os.mkdir(os.path.join(HTML_DIR, "old"))
    if not os.path.isdir(os.path.join(DWNLD_DIR, "old")):
        os.mkdir(os.path.join(DWNLD_DIR, "old"))

    basename = os.path.splitext(filename)[0]
    txt_file = os.path.join(TXT_DIR, basename + ".txt")
    html_file = os.path.join(HTML_DIR, basename + ".html")
    pdf_file = os.path.join(DWNLD_DIR, basename + ".pdf")
    txt_old_file = os.path.join(TXT_DIR, "old", basename + ".txt")
    html_old_file = os.path.join(HTML_DIR, "old", basename + ".html")
    pdf_old_file = os.path.join(DWNLD_DIR, "old", basename + ".pdf")
    os.rename(txt_file, txt_old_file)
    os.rename(html_file, html_old_file)
    os.rename(pdf_file, pdf_old_file)
    printc.red("Removing", filename, "because its old")


def find_old_files(spc_list, path):
    if len(years_list) != 1:
        printc.error("Year is not one", years_list)
        return

    year = years_list.pop()
    print(year)
    for filename in os.listdir(path):

        file = os.path.join(path, filename)
        if not os.path.isfile(file):
            continue
        txt = open_unknown_encoding_file(file)
        ch9 = get_chapter_text(txt, "9", file)
        ch10 = get_chapter_text(txt, "10", file)
        dates_str = ch9 + ch10
        # printc.yellow(dates_str)
        # print("-------------")
        dates = extract_dates(dates_str)
        is_old = False
        if not dates:
            printc.error("No date found! Check it manually")
            sublime(file)
            answer = query_yes_no("Is it old?", "no")
            if answer:
                is_old = True
        else:
            for date in dates:
                if date > year:
                    break
            else:
                is_old = True

        spc = find_spc_in_list(spc_list, filename)
        if is_old:
            move_old_files(filename)
            spc.status = status["old"]


def main():
    logger = init_settings()
    xls_filename = ""
    suffix = ""
    diff_files = False
    sublime_file = False
    rm = False
    far_csv = os.path.join(os.path.split(sys.argv[0])[0], "far.csv")
    global far_csv_glob
    far_csv_glob = far_csv
    csv_filename = ""

    parser = argparse.ArgumentParser(
        description="program for downloading a converting SPC documents")
    parser.add_argument("-c", "--clear", action="store_true",
                        help="clear everything in a folder except defined files when program starts")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="set logging level to DEBUG")
    parser.add_argument("--farlist", metavar="far_list.csv",
                        help="set another file for find-and-replace")
    parser.add_argument("-p", "--prettify", nargs="+", metavar="spc.html",
                        help="prettify html document")
    parser.add_argument("-s", "--suffix", metavar="suffix",
                        help="define suffix for prettify and html-to-txt functions")
    parser.add_argument("--diff", action="store_true",
                        help="launch diff program for old and new file")
    parser.add_argument("--sublime", action="store_true",
                        help="launch sublime text program for old and new file")
    parser.add_argument("--rm", action="store_true",
                        help="remove newly created files")
    parser.add_argument("--noviz", action="store_true",
                        help="turnoff viz body")
    parser.add_argument("--nochapter", action="store_true",
                        help="turnoff replacing chapters")
    parser.add_argument("--pattern", metavar="regex",
                        help="pattern for multiline grep")
    parser.add_argument("--list", metavar="spc_list_path",
                        help="add path for spc_list")
    main_args = parser.add_mutually_exclusive_group()
    main_args.add_argument("-x", "--xlstocsv", metavar="spc_list.xls[x]",
                           help="convert xls[x] to csv")
    main_args.add_argument("-a", "--all", metavar="spc_list.xls[x]",
                           help="run whole program from beggining to end (begin = xls file, end = downloaded PDFs and prepared TXT files")
    main_args.add_argument("-t", "--test", nargs="+", metavar="file",
                           help="not defined option for debugging purposes")
    main_args.add_argument("--test2", "--t2", nargs="+", metavar="file",
                           help="not defined option for debugging purposes")
    main_args.add_argument("--test3", "--t3", nargs="+", metavar="file",
                           help="not defined option for debugging purposes")
    main_args.add_argument("--htmltotxt", "--h2t", "--html2txt", nargs="+",
                           metavar="file.html",
                           help="convert html files to txt")
    main_args.add_argument("--htmltotxt2", "--h2t2", "--html2txt2", nargs="+",
                           metavar="file.html",
                           help="convert html files to txt")
    main_args.add_argument("--pdftohtml", "--p2h", "--pdf2html",
                           action="store_true",
                           help="convert pdf files to html through Word")
    main_args.add_argument("-f", "--far", nargs="+", metavar="file",
                           help="find and replace in files")
    main_args.add_argument("-r", "--reinsert", nargs="+", metavar="file",
                           help="reinsert tables in files")
    main_args.add_argument("--check", nargs="+", metavar="file",
                           help="check final spc if its ok")
    main_args.add_argument("--checkchapters", nargs="+", metavar="file",
                           help="check spc if has all chapters")
    main_args.add_argument("--checkbody", nargs="+", metavar="file",
                           help="check spc if has all chapters")
    main_args.add_argument("--replacebody", nargs="+", metavar="file",
                           help="check spc if has all chapters")
    main_args.add_argument("--replacechapters", nargs="+", metavar="file",
                           help="replace chapters")
    main_args.add_argument("--grep", nargs="+", metavar="file",
                           help="multiline grep")
    main_args.add_argument("--cleanhtml", nargs="+", metavar="file",
                           help="clean html")
    main_args.add_argument("--duplicates", action="store_true",
                           help="find duplicates")
    args = parser.parse_args()

    if args.suffix:
        suffix = args.suffix
    if args.list:
        spc_list = read_csv(args.list)
    if args.clear:
        clear_folder_except(["xls", "xslx", "py"])
    if args.farlist:
        far_csv = args.farlist
    if args.debug:
        logger.setLevel(logging.DEBUG)
    if args.diff:
        diff_files = True
    if args.sublime:
        sublime_file = True
    if args.rm:
        rm = True

    if args.xlstocsv:
        xls_filename = args.xlstocsv
        print(xls_filename)
        xls_to_csv(xls_filename)
    if args.all:
        do_it_all(args.all)
    if args.prettify:
        for file in args.prettify:
            html = open_unknown_encoding_file(file)
            html = prettify(html)
            save_as(html, file)
    if args.htmltotxt:
        for file in args.htmltotxt:
            html = open_unknown_encoding_file(file)
            txt = html_to_txt(html)
            save_as(txt, file, extension="txt")
    if args.htmltotxt2:
        for file in args.htmltotxt2:
            html = open_unknown_encoding_file(file)
            tables, html = process_all_tables(html)
            html = clean_html(html, far_csv)
            save_as(html, file)
            # html = find_and_replace_from_csv(far_csv, html, "test-html")
            txt = strip_html_tags(html)
            # txt = find_and_replace_from_csv(far_csv, txt, (
            #     "whitespaces", "typography", "txt-chars", "chapters", "experimental", "viz-body"))
            txt = find_and_replace_from_list(far_csv, txt, (
                "whitespaces", "typography", "txt-chars", "experimental",
                "viz-body"))
            txt = insert_tables(tables, txt)
            txt_file = save_as(txt, file, extension="txt")

    if args.far:
        for file in args.far:
            txt = open_unknown_encoding_file(file)
            txt = find_and_replace_from_list(far_csv, txt, ("cmd-line",))
            save_as(txt, file)
    if args.reinsert:
        for file in args.reinsert:
            try:
                html = open_unknown_encoding_file(
                    os.path.splitext(file)[0] + ".html")
            except FileNotFoundError as e:
                printc.error(str(e))
                html = open_unknown_encoding_file(
                    '../HTML/' + os.path.splitext(file)[0] + ".html")
            txt = open_unknown_encoding_file(file)
            txt = reinsert_table(txt, html)
            save_as(txt, file)
    if args.test:
        for file in args.test:
            original_html = open_unknown_encoding_file(file)
            html = find_and_replace_from_list(far_csv, original_html, (
                "html", "test", "html-chars", "test-html"))
            # html = remove_html_attrs(html)
            old_html_file = save_as(html, file, suffix="[old]")
            tables, html = process_all_tables(html)
            txt = strip_html_tags(html)
            txt = find_and_replace_from_list(far_csv, txt, (
                "whitespaces", "chapters", "experimental", "viz-body",
                "typography"))
            # txt = insert_tables(tables, txt)
            old_file = save_as(txt, file, suffix="[old]", extension="txt")

            html = find_and_replace_from_list(far_csv, original_html, (
                "html", "test", "html-chars", "test-html"))
            new_html_file = save_as(html, file, suffix="[new]")
            tables, html = process_all_tables(html)
            txt = strip_html_tags(html)
            txt = find_and_replace_from_list(far_csv, txt, (
                "whitespaces", "chapters", "experimental", "viz-body",
                "typography"))
            txt = insert_tables(tables, txt)
            new_file = save_as(txt, file, suffix="[new]", extension="txt")

            if sublime_file:
                sublime(old_html_file)
            if diff_files:
                diff(old_file, new_file)
                # diff(old_html_file, new_html_file)
            if rm:
                os.remove(old_file)
                os.remove(new_file)
                os.remove(old_html_file)
                os.remove(new_html_file)
    if args.test3:
        unique = set()
        for file in args.test3:
            html = open_unknown_encoding_file(file)
            for match in re.finditer("&.*?;", html):
                unique.add(match.group())
                # begin = match.start() - 30
                # m_begin = match.start()
                # m_end = match.end()
                # end = match.end() + 30
                # print(match.group(), end="")
                # printc.blue(html[begin:m_begin], end="")
                # printc.red(html[m_begin:m_end], end="")
                # printc.blue(html[m_end:end])
        test_array = []
        for u in unique:
            test_array.append(u)
            print(u)
        test = "\n".join(test_array)
        soup = BeautifulSoup(test)
        t2_array = soup.getText().split("\n")
        for i, x in enumerate(test_array):
            printc.red(test_array[i], end="")
            # printc.blue(t2_array[i])
    if args.check:
        for file in args.check:
            txt = open_unknown_encoding_file(file)
            row, col = check_final_spc(txt)
            while row:
                sublime(file + ":" + str(row) + ":" + str(col))
                input()
                txt = open_unknown_encoding_file(file)
                row, col = check_final_spc(txt)
            check_chapters(txt)
            multiline_grep(txt, "[^\d]\d\.\d[^\d]")
    if args.checkbody:
        for file in args.checkbody:
            txt = open_unknown_encoding_file(file)
            multiline_grep(txt, "[^\d]\d\.\d[^\d]")
            # txt = replace_viz_body(txt, far_csv)
    if args.replacebody:
        for file in args.replacebody:
            txt = open_unknown_encoding_file(file)
            txt = replace_viz_body(txt, far_csv)
            save_as(txt, file)
    if args.replacechapters:
        for file in args.replacechapters:
            txt = open_unknown_encoding_file(file)
            txt = find_and_replace_from_list(far_csv, txt, ("chapters",))
            save_as(txt, file)
    if args.checkchapters:
        for file in args.checkchapters:
            txt = open_unknown_encoding_file(file)
            ok = check_chapters(txt)
            if not ok:
                sublime(file)
                input()
    if args.pdftohtml:
        convert_to_html_with_word()
    if args.grep:
        if not args.pattern:
            printc.red("Need to provide pattern for grep")
        for file in args.grep:
            txt = open_unknown_encoding_file(file)
            multiline_grep(txt, args.pattern)
    if args.cleanhtml:
        for file in args.cleanhtml:
            html = open_unknown_encoding_file(file)
            html = clean_html(html, far_csv)
            html = find_and_replace_from_list(far_csv, html,
                                              ("html", "clean-html"))
            save_as(html, file, "!clean")
    if args.duplicates:
        duplicates_list = find_duplicates(DWNLD_DIR)
        print(duplicates_list)
    if args.test2:
        spc_list = read_csv("SPC27.csv")
        for spc in spc_list:
            if spc.status == status["converted_to_txt"]:
                if not os.path.isfile(os.path.join(TXT_DIR, spc.filename)):
                    printc.red(spc.filename, "not in folder")

if __name__ == "__main__":
    main()
