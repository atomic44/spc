__author__ = 'Atomic'

## Font attributes ##
# reset
reset = '\x1b[0m'  # off
default = '\x1b[39m'  # default foreground
bgdefault = '\x1b[49m'  # default background
#
bold = '\x1b[1m'  # bold
faint = '\x1b[2m'  # faint
standout = '\x1b[3m'  # standout
ul = '\x1b[4m'  # underlined
bk = '\x1b[5m'  # blink
rv = '\x1b[7m'  # reverse
hd = '\x1b[8m'  # hidden
nost = '\x1b[23m'  # no standout
noul = '\x1b[24m'  # no underlined
nobk = '\x1b[25m'  # no blink
norv = '\x1b[27m'  # no reverse
# colors
black = '\x1b[30m'
red = '\x1b[31m'
green = '\x1b[32m'
yellow = '\x1b[33m'
blue = '\x1b[34m'
magenta = '\x1b[35m'
cyan = '\x1b[36m'
white = '\x1b[37m'
dgray = '\x1b[90m'
lred = '\x1b[91m'
lgreen = '\x1b[92m'
lyellow = '\x1b[93m'
lblue = '\x1b[94m'
lmagenta = '\x1b[95m'
lcyan = '\x1b[96m'
lgray = '\x1b[97m'
# light colors

bgred = '\x1b[30m\x1b[41m'
bgblack = '\x1b[30m\x1b[40m'
bggreen = '\x1b[30m\x1b[42m'
bgyellow = '\x1b[30m\x1b[43m'
bgblue = '\x1b[30m\x1b[44m'
bgmagenta = '\x1b[30m\x1b[45m'
bgcyan = '\x1b[30m\x1b[46m'
bgwhite = '\x1b[30m\x1b[47m'
bgdgray = '\x1b[30m\x1b[100m'
bglred = '\x1b[30m\x1b[101m'
bglgreen = '\x1b[30m\x1b[102m'
bglyellow = '\x1b[30m\x1b[103m'
bglblue = '\x1b[30m\x1b[104m'
bglmagenta = '\x1b[30m\x1b[105m'
bglcyan = '\x1b[30m\x1b[106m'
bglgray = '\x1b[30m\x1b[107m'


# noinspection PyPep8Naming
class printc:
    @staticmethod
    def critical(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((bgred, string, reset)), end=end, file=file)

    @staticmethod
    def error(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((red, string, reset)), end=end, file=file)

    @staticmethod
    def warning(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((yellow, string, reset)), end=end, file=file)

    @staticmethod
    def info(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((blue, string, reset)), end=end, file=file)

    @staticmethod
    def debug(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((green, string, reset)), end=end, file=file)

    @staticmethod
    def red(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((red, string, reset)), end=end, file=file)

    @staticmethod
    def yellow(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((yellow, string, reset)), end=end, file=file)

    @staticmethod
    def blue(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((blue, string, reset)), end=end, file=file)

    @staticmethod
    def green(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((green, string, reset)), end=end, file=file)

    @staticmethod
    def magenta(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((magenta, string, reset)), end=end, file=file)

    @staticmethod
    def cyan(*args, sep=' ', end='\n', file=None):
        string = join_args(args, sep)
        print(''.join((cyan, string, reset)), end=end, file=file)


def join_args(args, sep):
    strings = []
    for arg in args:
        strings.append(str(arg))
    string = sep.join(strings)
    return string


